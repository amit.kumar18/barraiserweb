import { createStore, combineReducers } from "redux";

import { isDevBuild } from "src/utils";
import { currentUserReducer as currentUser } from "./currentUser";
import { appReducer as app } from "./app";
// import { workflowsReducer as workflows } from "modules/public-web/modules/workflows/redux";
// import { interviewFeedbackReducer as interviewFeedback } from "modules/public-web/modules/interview-feedback/redux";
// import { questionTaggingReducer as questionTagging } from "modules/public-web/modules/question-tagging/redux";
// import { adminReducer as admin } from "modules/public-web/modules/admin/redux";
// import { candidateEvaluationPlaygroundReducer as candidateEvaluationPlayground } from "modules/public-web/modules/candidate-evaluation-playground/redux";
// import { excerptGuidlinesReducer as excerptGuidlines } from "modules/public-web/modules/interview-structure/redux";
// import { excerptReducer as excerpt } from "modules/public-web/modules/excerpt/redux";
// import { interviewReducer as interview } from "modules/public-web/modules/interview/redux";
import { cmsReducer as cms } from "src/cms/redux";
// import { partnerCommentReducer as partnerComments } from "modules/public-web/modules/partner/redux";
// import { profileReducer as profile } from "modules/public-web/modules/profile/redux";
// import { jobRoleReducer as jobRole } from "modules/public-web/modules/job-role-form/redux";
// import { addCandidateFormReducer as addCandidateForm } from "modules/public-web/modules/add-candidate-form/redux";
// import { schedulingToolReducer as schedulingTool } from "modules/public-web/modules/scheduling-tool/redux";
// import { firepadReducer as firepad } from "../firepad/redux";

const IS_DEV = isDevBuild();

const rootReducer = combineReducers({
  app,
  currentUser,
  // workflows,
  // interviewFeedback,
  // questionTagging,
  // admin,
  // candidateEvaluationPlayground,
  // interview,
  // excerptGuidlines,
  // excerpt,
  cms,
  // partnerComments,
  // profile,
  // jobRole,
  // addCandidateForm,
  // schedulingTool,
  // firepad,
});

export const rootStore = createStore(
  rootReducer,
  IS_DEV &&
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
  window.__REDUX_DEVTOOLS_EXTENSION__()
);
