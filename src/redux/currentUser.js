export const CurrentUserActionStrings = {
  PHONE_OR_EMAIL_LOGIN_COMPLETE: "PHONE_OR_EMAIL_LOGIN_COMPLETE",
  SIGNUP_COMPLETE: "SIGNUP_COMPLETE",
  SET_USER_AUTHENTICATION: "SET_USER_AUTHENTICATION",
  SET_LOGGED_IN_USER_DETAILS: "SET_LOGGED_IN_USER_DETAILS",
  CLEAR_USER_DETAILS: "CLEAR_USER_DETAILS",
};

export const phoneOrEmailLoginCompleteAction = (payload = {}) => ({
  type: CurrentUserActionStrings.PHONE_OR_EMAIL_LOGIN_COMPLETE,
  payload,
});

export const signUpCompleteAction = (payload = {}) => ({
  type: CurrentUserActionStrings.SIGNUP_COMPLETE,
  payload,
});

export const setUserAuthentication = (payload = {}) => ({
  type: CurrentUserActionStrings.SET_USER_AUTHENTICATION,
  payload,
});

export const setLoggedInUserDetails = (payload = {}) => ({
  type: CurrentUserActionStrings.SET_LOGGED_IN_USER_DETAILS,
  payload,
});

export const clearUserDetailsAction = () => ({
  type: CurrentUserActionStrings.CLEAR_USER_DETAILS,
});

const INITIAL_STATE = {
  authChecked: false,
  userDetailsFetched: false,

  phoneNumberVerified: false,
  emailVerified: false,
  authenticated: false,
  phoneNumber: "",
  email: "",
  loggedInUserDetails: {},
  isInterviewer: false,
  isAdmin: false,
};

export const currentUserReducer = (state = INITIAL_STATE, action) => {
  const { type } = action;

  switch (type) {
    case CurrentUserActionStrings.PHONE_OR_EMAIL_LOGIN_COMPLETE: {
      const {
        payload: { verified, authenticated, phoneNumber, email } = {},
      } = action;
      return Object.assign({}, state, {
        phoneNumberVerified: verified && phoneNumber !== null,
        emailVerified: verified && email !== null,
        authenticated,
        phoneNumber,
        email,
      });
    }

    case CurrentUserActionStrings.SIGNUP_COMPLETE: {
      const { payload: { authenticated } = {} } = action;
      return Object.assign({}, state, {
        authenticated,
        authChecked: true,
      });
    }

    case CurrentUserActionStrings.SET_USER_AUTHENTICATION: {
      const { payload: { authenticated } = {} } = action;
      return Object.assign({}, state, {
        authenticated,
        authChecked: true,
      });
    }

    case CurrentUserActionStrings.SET_LOGGED_IN_USER_DETAILS: {
      const { payload = {} } = action;
      return Object.assign({}, state, {
        loggedInUserDetails: payload,
        isInterviewer: true,
        userDetailsFetched: true,
        isAdmin:
          payload.role === "admin" || payload.role === "ops" ? true : false,
      });
    }

    case CurrentUserActionStrings.CLEAR_USER_DETAILS: {
      return Object.assign({}, state, {
        ...INITIAL_STATE,
        authChecked: true,
      });
    }

    default:
      return state;
  }
};
