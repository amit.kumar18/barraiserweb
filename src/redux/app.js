const ActionStrings = {
  SET_IS_MOBILE_SCREEN: "SET_IS_MOBILE_SCREEN",
  SET_PARTNER_PORTAL_REFRESH_TIME_INTERVAL:
    "SET_PARTNER_PORTAL_REFRESH_TIME_INTERVAL",
};

export const setIsMobileScreenAction = (payload = false) => ({
  type: ActionStrings.SET_IS_MOBILE_SCREEN,
  payload,
});

export const setRefreshTimeAction = (payload = 1) => ({
  type: ActionStrings.SET_PARTNER_PORTAL_REFRESH_TIME_INTERVAL,
  payload: payload * 1000 * 60,
});

const INITIAL_STATE = {
  isMobileScreen: false,
  partnerPortalRefreshTimeInterval: 10 * 1000 * 60,
};

export const appReducer = (state = INITIAL_STATE, action) => {
  const { type } = action;

  switch (type) {
    case ActionStrings.SET_IS_MOBILE_SCREEN: {
      const { payload } = action;
      return Object.assign({}, state, {
        isMobileScreen: payload,
      });
    }

    case ActionStrings.SET_PARTNER_PORTAL_REFRESH_TIME_INTERVAL: {
      const { payload } = action;
      return Object.assign({}, state, {
        partnerPortalRefreshTimeInterval: payload,
      });
    }

    default:
      return state;
  }
};
