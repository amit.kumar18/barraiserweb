export const BUTTER_CMS_API_READ_KEY =
  "fd932099990a6dc3e2513968af084fe81a740f2b";

export const CMS_PAGE_NAMES = {
  HOME_PAGE: "home-page",
  BGS_ENQUIRY: "bgs-enquiry",
  COMMON: "common",
  CONTACT_PAGE: "contact-page",
  ABOUT_US: "about-us",
  EXPERT_PARTNER: "expert-partner",
  FOOTER: "footer",
  HEADER: "header",
  PRIVACY_POLICY: "privacy-policy",
  TERMS_CONDITIONS: "terms-conditions",
  CANDIDATE: "candidate",
  SCHEDULING_TOOL: "scheduling-tool",
};
