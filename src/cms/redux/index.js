export const CMSActionStrings = {
  ADD_PAGE: "ADD_PAGE",
};

export const addPageAction = (payload = {}) => ({
  type: CMSActionStrings.ADD_PAGE,
  payload,
});

const INITIAL_STATE = {
  pages: {},
};

export const cmsReducer = (state = INITIAL_STATE, action) => {
  const { type } = action;
  switch (type) {
    case CMSActionStrings.ADD_PAGE: {
      const { payload } = action;
      const stateCopy = JSON.parse(JSON.stringify(state));
      stateCopy.pages[payload["slug"]] = payload;
      return stateCopy;
    }
    default:
      return state;
  }
};
