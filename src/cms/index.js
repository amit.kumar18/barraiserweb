import React, { createContext, useContext, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";
import { safeGet } from "src/utils";
import { addPageAction } from "./redux";
import { BUTTER_CMS_API_READ_KEY } from "./constants";

import Butter from "buttercms";

const CMSContext = createContext();

const butter = Butter(BUTTER_CMS_API_READ_KEY);

const pageDataSelector = createStructuredSelector({
  pages: (state) => safeGet(state, ["cms", "pages"]),
});

const useLoadPageContentFromCMSCallback = (dispatch, pages) => {
  return (pageName) => {
    if (!pages[pageName]) {
      butter.page
        .retrieve("*", pageName, {
          locale: "en",
        })
        .then((resp) => {
          dispatch(addPageAction(resp.data.data));
        })
        .catch((resp) => {
          // TODO: handle failure
          console.log(resp);
        });
    }
  };
};

const useGetContentFromCMSCallback = (pages) => {
  return (pageName, contentPath) => {
    return safeGet(pages, [pageName, "fields", ...contentPath]);
  };
};

export function CMSProvider({ children }) {
  const dispatch = useDispatch();
  const { pages } = useSelector(pageDataSelector);
  const loadPageContentFromCMS = useLoadPageContentFromCMSCallback(
    dispatch,
    pages
  );
  const getContentFromCMS = useGetContentFromCMSCallback(pages);

  return (
    <CMSContext.Provider
      value={{
        loadPageContentFromCMS,
        getContentFromCMS,
      }}
    >
      {children}
    </CMSContext.Provider>
  );
}

export function useCMS() {
  const context = useContext(CMSContext);
  if (context === undefined) {
    throw new Error("useCMS must be used within a CMSProvider");
  }
  return context;
}

export * from "./constants";
