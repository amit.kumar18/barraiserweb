export const ROUTE_PREFIXES = {
  jobRoleForm: "/job-role",
  excerpt: "/excerpts",
  interviewStructure: "/is",
  candidateInterviewStructure: "/is/c",
  expertInterviewStructure: "/is/e",
  home: "/home",
  aboutUs: "/about-us",
  expertPartner: "/expert-partner",
  contactUs: "/contact-us",
  scheduleInterview: "/schedule-interview",
  privacyPolicy: "/legal/privacy-policy",
  termsConditions: "/legal/terms-conditions",
  homeRoutes: {
    howItWorks: "/home/how-it-works",
    resume: "/home/resume",
    whyBarRaiser: "/home/why-barraiser",
    stories: "/home/stories",
    faqs: "/home/faqs",
  },
  signup: "/signup",
  personalDetails: "/personal-details",
  workflows: "/wf",
  paymentStatus: "/payment-status",
  interviewerAvailability: "/interviewer-availability",
  candidateEvaluation: "/candidate-evaluation",
  interviewConfirmationCandidate: "/interview-confirmation/candidate",
  profile: "/profile",
  scheduling: "/scheduling",
  interviewing: "/interviewing",
  bgsEnquiry: "/bgs-enquiry",
  interviewingInterviewer: "/interview-landing/e",
  interviewingCandidate: "/interview-landing/c",
  partner: "/partner",
  candidate: "/candidate",
  interviewFeedback: "/interview-feedback",
  interviewQC: "/interview-qc",
  questionTagging: "/question-tagging",
  jobDetails: "/job-details",
  schedulingTool: "/scheduling-tool",
};

export const API_URI_BASE =
  process.env.REACT_APP_BUILD_ENV === "production"
    ? "https://api.barraiser.com/"
    : process.env.REACT_APP_BUILD_ENV === "staging"
    ? "http://api.staging.barraiser.in/"
    : "/";

export const API_URI_GQL = `${API_URI_BASE}graphql`;
