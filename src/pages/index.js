import React, { useEffect, useState } from "react";
import {
  AppMenuBar,
  BookADemoForm,
  HeroContainer,
  LiveStatistics,
  BRFooter,
  BOOK_DEMO_CONTEXT_TYPE,
  TestimonialWidget,
} from "modules/components";

import {
  CompanyLogos,
  InterviewProcessEngineering,
  StructuredEvaluationProcess,
  InterviewsImages,
  ClientTestimonials,
  IndustryExpertSays,
  Candidatesays,
  CheckoutSection,
  CaseStudy,
} from "../modules/homepage/components";

import {
  Container,
  makeStyles,
  Dialog,
  Box,
  Typography,
  Button,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Helmet } from "react-helmet";

import { useCMS, CMS_PAGE_NAMES } from "../cms";
// import { FETCH_KPI_DATA } from "../graphql";
// import { useLazyQuery } from "@apollo/react-hooks";
// import { safeGetViaSchema } from "src/utils";

const useStyles = makeStyles((theme) => ({
  root: {
    overflowX: "hidden",
  },
  image: {
    flexGrow: 1,
    width: "100%",
  },
  seperator: {
    background: "#0898A9",
    height: 27.9,
    [theme.breakpoints.down("sm")]: {
      height: 4,
    },
    [theme.breakpoints.up("xl")]: {
      height: 20,
    },
    width: "100%",
  },
  candidateSaysHeader: {
    width: "44.65vw",
    paddingBottom: 28,
    [theme.breakpoints.up("xl")]: {
      width: "33.48vw",
      paddingBottom: 0,
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      paddingBottom: 0,
    },
    marginLeft: "auto",
    marginRight: "auto",
  },
}));

function IndexPage() {
  const { getContentFromCMS, loadPageContentFromCMS } = useCMS();
  loadPageContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isRegular = useMediaQuery(theme.breakpoints.up("md"));

  const classes = useStyles();
  const [isModalOpen, setIsModalOpen] = useState(false);
  // const [kpis, setKpis] = useState(null);

  const numberOfPartners = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "businesses_onboard",
  ]);
  const numberOfInterviews = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "candidates_interviewed",
  ]);
  const numberOfExperts = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "expert_partnered",
  ]);

  const candidateSays = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "candidate_says",
  ]);

  // const [fetchKPIDataAPI, { loading: fetchingKPIData }] = useLazyQuery(
  //   FETCH_KPI_DATA,
  //   {
  //     onCompleted: ({ getDisplayKPINumbers: data }) => {
  //       data && setKpis(safeGetViaSchema(data, {}));
  //     },
  //     onError: (err) => {
  //       console.error("error: ", err);
  //     },
  //   }
  // );

  const bookDemoHandler = () => {
    setIsModalOpen(true);
  };

  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const closeFormHandler = () => {
    setIsModalOpen(false);
    setSuccessMessage("");
  };

  // useEffect(() => {
  //   // fetchKPIDataAPI();
  // }, []);

  return (
    <Container maxWidth={false} className={classes.root}>
      <Helmet>
        <title>BarRaiser HomePage</title>
        <meta name="description" content="Welcome to BarRaiser" />
      </Helmet>
      <AppMenuBar />
      <HeroContainer onClick={bookDemoHandler} />
      <LiveStatistics
        data={{
          numberOfPartners,
          numberOfInterviews,
          numberOfExperts,
        }}
      />
      <CaseStudy />
      <CompanyLogos onClick={bookDemoHandler} />
      <Container maxWidth={false} className={classes.seperator}></Container>
      <InterviewProcessEngineering onClick={bookDemoHandler} />
      <StructuredEvaluationProcess />
      <InterviewsImages />
      <ClientTestimonials />
      <IndustryExpertSays />
      <TestimonialWidget testimonials={candidateSays}>
        <Typography
          variant="h2"
          color="inherit"
          gutterBottom
          className={classes.candidateSaysHeader}
        >
          {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
            "candidate_says_header",
          ])}
        </Typography>
      </TestimonialWidget>
      <CheckoutSection />
      <Dialog
        aria-labelledby="modal-title"
        modal={true}
        open={isModalOpen}
        onClose={() => setIsModalOpen(false)}
      >
        <Box p={10} pt={20} pb={20} width={isRegular ? "600px" : "auto"} maxWidth="90%">
          {successMessage ? (
            <Box textAlign="center">
              <Typography variant="h3" color="primary">
                Thank you
              </Typography>
              <Typography variant="h4">
                <Box fontWeight={500} color="#414141" mt={2} mb={2}>
                  for submitting your details.
                </Box>
              </Typography>
              <Typography variant="body1" color="#414141">
                <Box fontWeight={400} mb={2}>
                  Our team will connect with you shortly.
                </Box>
              </Typography>

              <Button
                variant="contained"
                color="primary"
                onClick={closeFormHandler}
                size="small"
              >
                Close
              </Button>
            </Box>
          ) : (
            <>
              <BookADemoForm
                name
                email
                phone={{ optional: true }}
                organisation
                submitButtonText="Book a Demo"
                context={BOOK_DEMO_CONTEXT_TYPE.BOOK_A_DEMO}
                onSuccess={(message) => {
                  setSuccessMessage(message);
                }}
                onError={(message) => {
                  setIsModalOpen(false);
                  setErrorMessage(message);
                }}
              />
            </>
          )}
        </Box>
      </Dialog>
      <Dialog
        aria-labelledby="modal-title"
        modal={true}
        open={!!errorMessage}
        onClose={() => setErrorMessage("")}
      >
        <Typography variant="h5">
          <Box pt={4} textAlign="center">
            Error
          </Box>
        </Typography>
        <Box p={8} color={theme.palette.error.main}>
          {errorMessage}
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setErrorMessage("")}
          size="small"
        >
          Close
        </Button>
      </Dialog>
      <BRFooter />
    </Container>
  );
}

export default IndexPage;
