import { createMuiTheme, responsiveFontSizes } from "@material-ui/core";

export const Breakpoints = {
    xs: "480px",
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px",
    xxl: "1600px",
};

export const Fonts = {
    default: "Montserrat",
};

export const FontWeights = {
    default: "400",
    semibold: "500",
    bold: "700",
};

export const FontSizes = {
    lgHeader: "64px",
    h1: "44px",
    h2: "40px",
    h3: "32px",
    xlText: "24px",
    largeText: "20px",
    mediumText: "18px",
    default: "16px",
    mobileDefault: "14px",
    smallText: "12px",
};

export const Colors = {
    white: "#fff",
    offWhite: "#f8f8f8",
    yellow: "#f7c84e",
    grey: "#414141",
    lightGrey: "#dbdbdb",
    black: "#000",
    purple: "#4457e5",
    blue: "#3c75b9",
    darkBlue: "#0115aa",
    green: "#19ab00",
    red: "#d00000",
};

export const Palette = {
    text: {
        white: Colors.white,
        default: Colors.grey,
        brandLogo: Colors.purple,
        black: Colors.black,
        green: Colors.green,
        red: Colors.red,
    },
    background: {
        purple: Colors.purple,
        white: Colors.white,
        yellow: Colors.yellow,
        lightGrey: Colors.lightGrey,
        blue: Colors.blue,
        darkBlue: Colors.darkBlue,
        offWhite: Colors.offWhite,
        lightBlue: "#eceffb",
        none: "transparent",
    },
    border: {
        white: Colors.white,
        buttonGreyBorder: "#979797",
        purple: Colors.purple,
    },
};

export const Gradients = {
    homeBlue: "linear-gradient(to left, #6d88ff, #2a43ff)",
};

export const Spacing = {
    0: "0px",
    4: "4px",
    8: "8px",
    12: "12px",
    16: "16px",
    24: "24px",
    32: "32px",
    40: "40px",
    48: "48px",
};

export const BorderRadius = {
    default: "4px",
    50: "50px",
    half: "50%",
    "1/4": "25%",
    "1/8": "12.5%",
    round: "100%",
};

// export const Theme = {
//     BorderRadius,
//     Breakpoints,
//     FontWeights,
//     Fonts,
//     FontSizes,
//     Gradients,
//     Palette,
//     Spacing,
// };

export const BRTheme = createMuiTheme({
    breakpoints: {
        xs: "360px",
        sm: "550px",
        md: "800px",
        lg: "1200px",
        xl: "1500px",
    },
    mixins: {
        gutters: [],
        toolbar: {
            minHeight: 72,
        },
        formTextLabel: {
            color: "#A19F9D",
            fontWeight: 400,
            fontSize: 14,
            lineHeight: 11 / 7,
        },
    },
    shadows: [
        "none",
        "0px 1px 1px rgba(216, 216, 216, 0.14), 0px 2px 1px rgba(216, 216, 216, 0.12), 0px 1px 3px rgba(216, 216, 216, 0.2)",
        "0px 2px 2px rgba(216, 216, 216, 0.14), 0px 3px 1px rgba(216, 216, 216, 0.12), 0px 1px 5px rgba(216, 216, 216, 0.2)",
        "0px 3px 4px rgba(216, 216, 216, 0.14), 0px 3px 3px rgba(216, 216, 216, 0.12), 0px 1px 8px rgba(216, 216, 216, 0.2)",
        "0px 4px 5px rgba(216, 216, 216, 0.14), 0px 1px 10px rgba(216, 216, 216, 0.12), 0px 2px 4px rgba(216, 216, 216, 0.2)",
        "0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)",
        "0px 6px 10px rgba(216, 216, 216, 0.14), 0px 1px 18px rgba(216, 216, 216, 0.12), 0px 3px 5px rgba(216, 216, 216, 0.2)",
        "0px 4px 5px -2px rgba(0,0,0,0.2),0px 7px 10px 1px rgba(0,0,0,0.14),0px 2px 16px 1px rgba(0,0,0,0.12)",
        "0px 8px 10px rgba(216, 216, 216, 0.14), 0px 3px 14px rgba(216, 216, 216, 0.12), 0px 5px 5px rgba(216, 216, 216, 0.2)",
        "0px 9px 12px rgba(216, 216, 216, 0.14), 0px 3px 16px rgba(216, 216, 216, 0.12), 0px 5px 6px rgba(216, 216, 216, 0.2)",
        "0px 6px 6px -3px rgba(0,0,0,0.2),0px 10px 14px 1px rgba(0,0,0,0.14),0px 4px 18px 3px rgba(0,0,0,0.12)",
        "0px 6px 7px -4px rgba(0,0,0,0.2),0px 11px 15px 1px rgba(0,0,0,0.14),0px 4px 20px 3px rgba(0,0,0,0.12)",
        "0px 12px 17px rgba(216, 216, 216, 0.14), 0px 5px 22px rgba(216, 216, 216, 0.12), 0px 7px 8px rgba(216, 216, 216, 0.2)",
        "0px 7px 8px -4px rgba(0,0,0,0.2),0px 13px 19px 2px rgba(0,0,0,0.14),0px 5px 24px 4px rgba(0,0,0,0.12)",
        "0px 7px 9px -4px rgba(0,0,0,0.2),0px 14px 21px 2px rgba(0,0,0,0.14),0px 5px 26px 4px rgba(0,0,0,0.12)",
        "0px 8px 9px -5px rgba(0,0,0,0.2),0px 15px 22px 2px rgba(0,0,0,0.14),0px 6px 28px 5px rgba(0,0,0,0.12)",
        "0px 12px 17px rgba(216, 216, 216, 0.14), 0px 5px 22px rgba(216, 216, 216, 0.12), 0px 7px 8px rgba(216, 216, 216, 0.2)",
        "0px 8px 11px -5px rgba(0,0,0,0.2),0px 17px 26px 2px rgba(0,0,0,0.14),0px 6px 32px 5px rgba(0,0,0,0.12)",
        "0px 9px 11px -5px rgba(0,0,0,0.2),0px 18px 28px 2px rgba(0,0,0,0.14),0px 7px 34px 6px rgba(0,0,0,0.12)",
        "0px 9px 12px -6px rgba(0,0,0,0.2),0px 19px 29px 2px rgba(0,0,0,0.14),0px 7px 36px 6px rgba(0,0,0,0.12)",
        "0px 10px 13px -6px rgba(0,0,0,0.2),0px 20px 31px 3px rgba(0,0,0,0.14),0px 8px 38px 7px rgba(0,0,0,0.12)",
        "0px 10px 13px -6px rgba(0,0,0,0.2),0px 21px 33px 3px rgba(0,0,0,0.14),0px 8px 40px 7px rgba(0,0,0,0.12)",
        "0px 10px 14px -6px rgba(0,0,0,0.2),0px 22px 35px 3px rgba(0,0,0,0.14),0px 8px 42px 7px rgba(0,0,0,0.12)",
        "0px 11px 14px -7px rgba(0,0,0,0.2),0px 23px 36px 3px rgba(0,0,0,0.14),0px 9px 44px 8px rgba(0,0,0,0.12)",
        "0px 24px 38px rgba(216, 216, 216, 0.14), 0px 9px 46px rgba(216, 216, 216, 0.12), 0px 11px 15px rgba(216, 216, 216, 0.2)",
    ],
    overrides: {
        MuiButton: {
            root: {},
            sizeSmall: {
                width: 96,
                height: 36,
                margin: "0.5rem",
                borderRadius: "0.35rem",
            },
            sizeLarge: {
                height: 64,
                width: 312,
                textTransform: "none",
                borderRadius: 6,
                maxWidth: "100%",
                "@media (min-width: 600px)": {
                    width: 350,
                },
                "@media (max-width:350px)": {
                    width: "100%",
                },
            },
            outlinedPrimary: {
                backgroundColor: "#fff",
                "&:hover": {
                    backgroundColor: "#f8f8f8",
                },
            },
            contained: {
                boxShadow: "none",
                "@media (min-width: 600px)": {
                    width: 330,
                },
            },
        },

        MuiContainer: {
            root: {
                fontFamily: "Roboto",
                paddingLeft: 0,
                paddingRight: 0,
                "@media (min-width: 600px)": {
                    paddingLeft: 0,
                    paddingRight: 0,
                },
            },
        },
        MuiToolbar: {
            root: {
                width: 1280,
                margin: "auto",
                padding: 0,
                "@media (max-width: 1300px)": {
                    width: "90%",
                },
            },
        },
        MuiLink: {
            root: {
                margin: "0 1rem",
                textTransform: "capitalize",
            },
        },
        MuiFormLabel: {
            root: {
                fontSize: 18,
                fontWeight: 400,
                color: "rgba(0, 0, 0, 0.6)",
            },
        },
        MuiSlider: {
            track: {
                height: 4,
                borderRadius: "2px",
            },
            rail: {
                height: 4,
                borderRadius: "2px",
            },
            thumb: {
                height: 13,
                width: 13,
            },
            mark: {
                display: "none",
            },
        },
        MuiInputBase: {
            root: {
                borderRadius: 6,
            },
        },
        MuiTypography: {
            gutterBottom: {
                marginBottom: 40,
            },
        },
        MuiCard: {
            root: {
                borderRadius: 8,
                padding: "24px 14px 33px 24px",
                "@media (max-width: 600px)": {
                    padding: "24px 25px 22px 24px",
                },
            },
            rounded: {
                borderRadius: 10,
            },
        },
        MuiAvatar: {
            root: {
                width: 56,
                height: 56,
            },
        },
    },
    palette: {
        common: { black: "#000", white: "#fff" },
        type: "light",
        primary: {
            light: "#58c9db",
            main: "#0898A9",
            dark: "#006a7a",
            contrastText: "#fff",
        },
        secondary: {
            light: "#356272",
            main: "#003847",
            dark: "#001320",
            contrastText: "#fff",
        },
        grey: {
            300: "#FAFAFA",
            500: "#909090",
            700: "#606060",
            900: "#121212",
        },
        text: {
            primary: "#737373",
            secondary: "#414141",
        },
        colors: Colors,
    },
    typography: {
        fontFamily: '"Roboto"',
        fontSizes: FontSizes,
        fontWeights: FontWeights,
        fonts: Fonts,
        opacity: 0.87,
        h1: {
            fontSize: 56,
            lineHeight: 72 / 56,
            fontWeight: 500,
            letterSpacing: -1.5,
        },
        h2: {
            fontSize: 48,
            lineHeight: 1.5,
            fontWeight: 300,
            letterSpacing: -0.5,
        },
        h3: {
            fontSize: 36,
            lineHeight: 56 / 36,
            fontWeight: 500,
        },
        h4: {
            fontSize: 28,
            lineHeight: 9 / 7,
            fontWeight: 500,
        },
        h5: {
            fontSize: 20,
            lineHeight: 1.4,
            letterSpacing: 0.18,
            fontWeight: 700,
        },
        h6: {
            fontSize: 18,
            lineHeight: 4 / 3,
            letterSpacing: 0.15,
            fontWeight: 500,
        },
        subtitle1: {
            fontSize: 16,
            lineHeight: 1.5,
            letterSpacing: 0.15,
            fontWeight: 500,
        },
        subtitle2: {
            fontSize: 14,
            lineHeight: 24 / 14,
            letterSpacing: 0.1,
            fontWeight: 500,
        },
        body1: {
            fontFamily: "Open Sans",
            fontSize: 16,
            fontWeight: 400,
            lineHeight: 1.5,
        },
        body2: {
            fontFamily: "Open Sans",
            color: "#737373",
            fontSize: 14,
            lineHeight: 10 / 7,
            fontWeight: 400,
            letterSpacing: 0.25,
        },
        button: {
            fontSize: 14,
            lineHeight: 8 / 7,
            letterSpacing: 1.25,
            fontWeight: 500,
            textTransform: "capitalize",
        },
        caption: {
            fontSize: 12,
            lineHeight: 1.33,
            letterSpacing: 0.4,
            fontWeight: 400,
        },
        overline: {
            fontSize: 10,
            lineHeight: 1.6,
            letterSpacing: 1.5,
            textTransform: "uppercase",
            fontWeight: 500,
        },
    },
    spacing: (value) => value * 4,
    shape: { borderRadius: BorderRadius },
});
