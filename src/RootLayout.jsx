import { CMSProvider } from "./cms";
import React from "react";
import { Helmet } from "react-helmet";
import { Provider as ReduxProvider } from "react-redux";
import { rootStore } from "src/redux";
import { MuiThemeProvider } from "@material-ui/core";
import { BRTheme } from "src/theme";
import { ApolloProvider } from "@apollo/react-hooks";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createHttpLink } from "apollo-link-http";
import { isDevBuild } from "src/utils";
import { API_URI_GQL } from "constantVars";

const IS_DEV = isDevBuild();
const cache = new InMemoryCache();
const client = new ApolloClient({
    link: createHttpLink({ uri: API_URI_GQL, credentials: "include" }),
    cache,
    queryDeduplication: false,
    defaultOptions: {
        watchQuery: {
            fetchPolicy: "no-cache",
            errorPolicy: "ignore",
        },
        query: {
            fetchPolicy: "no-cache",
            errorPolicy: "all",
        },
    },
    ...(IS_DEV ? {} : { connectToDevTools: true }),
});

export default function RootLayout({ children }) {
    return (
        <>
            <Helmet>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="theme-color" content="#4457e5" />
                <link rel="icon" href="%PUBLIC_URL%/logo.svg" />
                <meta name="description" content="BarRaiser" />
                <meta property="og:title" content="BarRaiser" />
                <meta property="og:description" content="10x greater chance of landing your dream job" />
                <meta property="og:image" content="%PUBLIC_URL%/br-social.png" />
                <meta property="og:url" content="https://www.barraiser.in" />
                <meta name="twitter:card" content="summary_large_image" />
                <title>BarRaiser</title>
            </Helmet>
            <ApolloProvider client={client}>
                <ReduxProvider store={rootStore}>
                    <CMSProvider>
                        <MuiThemeProvider theme={BRTheme}>
                            {children}
                        </MuiThemeProvider>
                    </CMSProvider>
                </ReduxProvider >
            </ApolloProvider>
        </>
    )
}
