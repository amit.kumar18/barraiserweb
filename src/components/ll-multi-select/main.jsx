import React from "react";
import styled from "styled-components";
import { Flex } from "src/components";

const StyledMultiSelectContainer = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Palette, FontSizes, Spacing, BorderRadius } = theme;
    return {
      overflow: "hidden",
      height: props.height || 360,
      width: props.width || 500,
      boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.13)",
      background: Palette.background.white,
      fontSize: FontSizes.mobileDefault,
      paddingBottom: Spacing[24],
      borderRadius: BorderRadius.default,

      ".toolbar-container": {
        padding: Spacing[12],
      },

      ".body-container": {
        overflow: "hidden;",
        flex: "1 1 auto",
      },

      ".body": {
        flex: "1 1 auto",
        padding: Spacing[12],
        overflow: "auto",
        borderRight: "1px solid #e2e2e2",
        minWidth: "60%",
      },

      ".rhs-container": {
        padding: Spacing[12],
        overflow: "auto",
        minWidth: "40%",
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        padding: Spacing[12],
        boxShadow: "none",
        paddingBottom: 0,
        width: "100%",
        height: "100%",
        borderRadius: "none",

        ".toolbar-container": {
          padding: 0,
          paddingBottom: Spacing[12],
        },
        ".body-container": {
          flexDirection: "column",
        },

        ".body": {
          padding: 0,
          paddingBottom: Spacing[12],
          borderRight: 0,
          borderBottom: "1px solid #e2e2e2",
          width: "100%",
          minHeight: "60%",
        },
        ".rhs-container": {
          padding: 0,
          paddingTop: Spacing[12],
          width: "100%",
          minHeight: "150px",
        },
      },
    };
  }}
`;

export function LLMultiSelectMain({
  children,
  selectionDisplayList,
  toolbar,
  actionBar,
  width,
  height,
}) {
  return (
    <StyledMultiSelectContainer
      inline
      flexDirection="column"
      width={width}
      height={height}
    >
      <Flex className="toolbar-container">{toolbar}</Flex>

      <Flex className="body-container">
        <Flex className="body" flexDirection="column" flex={1}>
          {children}
        </Flex>

        <Flex className="rhs-container" flexDirection="column">
          {selectionDisplayList}
        </Flex>
      </Flex>
      {actionBar}
    </StyledMultiSelectContainer>
  );
}
