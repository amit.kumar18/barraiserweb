import React from "react";
import styled from "styled-components";
import { Flex, Icon } from "src/components";

const StyledItem = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { Spacing } = theme;
    return {
      marginBottom: Spacing[8],
      cursor: "pointer",
      ".icon": {
        // color: 'red'
      },
      ".display-text": {},
    };
  }}
`;

export function LLSelectionDisplayList({
  data,
  onCrossClick,
  className,
  labelKey,
}) {
  if (!data || !data.length || !Array.isArray(data)) {
    return null;
  }

  const handleCrossClick = (obj) => (e) => {
    onCrossClick(obj);
  };

  return data.map((obj) => {
    return (
      <StyledItem
        alignItems="center"
        onClick={handleCrossClick(obj)}
        className={`${className}`}
      >
        <Icon type="CloseCircleFilled" className="icon" />
        <div className="display-text ml8">{obj[labelKey]}</div>
      </StyledItem>
    );
  });
}
