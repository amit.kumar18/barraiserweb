import React from "react";
import styled from "styled-components";
import { Checkbox } from "src/components";

const StyledCheckbox = styled(Checkbox)`
  ${(props) => {
    const { theme } = props;
    const { Spacing } = theme;
    return {
      margin: 0,
      marginBottom: Spacing[8],

      "&.ant-checkbox-wrapper + .ant-checkbox-wrapper": {
        margin: 0,
        marginBottom: Spacing[8],
      },
    };
  }}
`;

export function LLOptionsContainer({
  data,
  value = [],
  valueKey,
  labelKey,
  onChange,
  className,
}) {
  if (!data || !data.length || !Array.isArray(data)) {
    return null;
  }

  const onCheckBoxChange = (obj) => {
    return (e) => {
      onChange(obj, e.target.checked);
    };
  };

  const checkIfChecked = (obj) => {
    return value.indexOf(obj[valueKey]) !== -1;
  };

  return data.map((obj, index) => {
    const optionObj = {
      [labelKey]: obj[labelKey],
      [valueKey]: obj[valueKey],
    };
    return (
      <StyledCheckbox
        key={`multiSelectOptions-${index}`}
        className={className}
        onChange={onCheckBoxChange(optionObj)}
        checked={checkIfChecked(optionObj)}
      >
        {optionObj[labelKey]}
      </StyledCheckbox>
    );
  });
}
