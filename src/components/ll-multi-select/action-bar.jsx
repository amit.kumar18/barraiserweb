import React from "react";
import styled from "styled-components";
import { Flex } from "src/components";

const StyledMultiSelectContainer = styled(Flex)``;

const StyledOverflowContainer = styled(Flex)`
  overflow: auto;
`;

export function LLActionBar({ children }) {
  return children;
}
