import styled from "styled-components";

const StyledLineButton = styled.button`
  ${props => {
    const { theme, active } = props;
    const { Palette, FontSizes, FontWeights, BorderRadius, Spacing } = theme;
    return {
      display: "inline-flex",
      alignItems: "center",
      justifyContent: "center",
      padding: `${Spacing[4]} ${Spacing[16]}`,
      borderRadius: BorderRadius[50],
      border: active ? "none" : `1px solid ${Palette.border.buttonGreyBorder}`,
      backgroundColor: active
        ? Palette.background.yellow
        : Palette.background.white,
      cursor: "pointer",
      fontSize: FontSizes.smallText,
      fontWeight: active ? FontWeights.bold : FontWeights.default,
      color: Palette.text.default,
      "&:hover": {
        textDecoration: "underline"
      }
    };
  }}
`;

export const LineButton = StyledLineButton;
