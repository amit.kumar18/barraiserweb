import styled from "styled-components";

const variantSpecificStyles = ({ Palette }, variant = "default") => {
  const specificProps = {
    primary: {
      backgroundColor: Palette.background.blue,
      color: Palette.text.white,
    },
    secondary: {
      color: Palette.text.default,
      backgroundColor: Palette.background.yellow,
    },
    tertiary: {
      color: Palette.text.black,
      backgroundColor: Palette.background.white,
    },
    default: {
      color: Palette.text.default,
      backgroundColor: Palette.background.lightGrey,
    },
  };
  return specificProps[variant];
};

const sizeSpecificStylesObj = ({ FontSizes }, size) => {
  const styles = {
    lg: {
      height: "56px",
      width: "338px",
      fontSize: FontSizes.mediumText,
    },
    md: {
      padding: "12px 20px",
      fontSize: FontSizes.mobileDefault,
    },
  };
  return styles[size];
};

const disabledButtonSpecificProps = (disabled) => {
  return disabled
    ? {
        opacity: 0.5,
      }
    : {};
};

const StyledBrandLogo = styled.button`
  ${(props) => {
    const { theme, variant = "default", size = "lg", disabled = false } = props;
    const { FontWeights, BorderRadius } = theme;
    const varSpecProps = variantSpecificStyles(theme, variant);
    const sizeSpecificStyles = sizeSpecificStylesObj(theme, size);
    const disabledProps = disabledButtonSpecificProps(disabled);
    return {
      ...varSpecProps,
      ...sizeSpecificStyles,
      ...disabledProps,
      display: "inline-flex",
      alignItems: "center",
      justifyContent: "center",
      fontWeight: FontWeights.bold,
      borderRadius: BorderRadius[50],
      border: "none",
      cursor: "pointer",
      maxWidth: "100%",
      outline: "none",
      "&:hover": {
        textDecoration: "underline",
      },
    };
  }}
`;

export const BrandButton = StyledBrandLogo;
