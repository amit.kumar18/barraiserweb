import styled from "styled-components";

export const Flex = styled.div`
  ${(props) => {
    const {
      inline,
      alignItems,
      justifyContent,
      flex,
      flexGrow,
      flexDirection,
      flexWrap,
    } = props;
    return {
      display: inline ? "inline-flex" : "flex",
      ...(flexDirection && { flexDirection }),
      ...(alignItems && { alignItems }),
      ...(justifyContent && { justifyContent }),
      ...(flex && { flex }),
      ...(flexGrow && { flexGrow }),
      ...(flexWrap && { flexWrap }),
    };
  }}
`;
