import React from "react";
import moment from "moment";
import { Flex, Icon } from "src/components";

import styled from "styled-components";

export const StyledStopWatch = styled.div`
  ${(props) => {
    const { theme } = props;
    const { FontSizes, FontWeights } = theme;
    return {
      ".timer": {
        fontSize: FontSizes.h3,
        fontWeight: FontWeights.bold,
      },
      ".actions": {
        fontSize: FontSizes.xlText,
        cursor: "pointer",
      },
    };
  }}
`;

const REFRESH_INTERVAL = 1000;

const INITIAL_VAL = moment().set({
  hour: 0,
  minute: 0,
  second: 0,
  millisecond: 0,
});

export class StopWatch extends React.Component {
  intervalId;
  value = moment(INITIAL_VAL);

  constructor(props) {
    super(props);
    this.state = {
      played: false,
    };
  }

  startTimer = () => {
    if (this.intervalId) return;

    this.intervalId = window.setInterval(() => {
      // Updating value
      this.value = this.value.add(1, "second");

      // Syncing the change
      this.props.onChange && this.props.onChange(this.value);

      // Force update
      this.forceUpdate();
    }, REFRESH_INTERVAL);

    this.setState({
      played: true,
    });
  };

  pauseTimer = () => {
    if (!this.intervalId) {
      return null;
    }

    // clearing timer
    clearInterval(this.intervalId);
    this.intervalId = null;

    // setting state
    this.setState({
      played: false,
    });
  };

  resetTimer = () => {
    clearInterval(this.intervalId);

    // clearing timer
    this.intervalId = null;
    this.value = moment(INITIAL_VAL);

    // setting state
    this.setState({
      played: false,
    });
  };

  render() {
    const { className = "" } = this.props;
    return (
      <StyledStopWatch className={`${className}`}>
        <div className="timer">{this.value.format("HH : mm : ss")}</div>
        <Flex className="actions" justifyContent="center">
          {this.state.played ? (
            <div onClick={this.pauseTimer}>
              <Icon type="PauseCircleOutlined" />
            </div>
          ) : (
            <div onClick={this.startTimer}>
              <Icon type="PlayCircleOutlined" />
            </div>
          )}
          <div onClick={this.resetTimer} className="pl12">
            <Icon type="CloseCircleOutlined" />
          </div>
        </Flex>
      </StyledStopWatch>
    );
  }
}
