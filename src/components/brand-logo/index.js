import React from "react";
import styled from "styled-components";

const StyledBrandLogo = styled.div`
  ${props => {
    const { theme } = props;
    const { Palette, FontWeights, FontSizes } = theme;
    return {
      display: "inline-flex",
      color: Palette.text.brandLogo,
      fontWeight: FontWeights.bold,
      fontSize: FontSizes.mediumText
    };
  }}
`;

export function BrandLogo() {
  return <StyledBrandLogo>BarRaiser</StyledBrandLogo>;
}
