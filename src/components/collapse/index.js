import React, { useState } from "react";
import styled from "styled-components";

export const Collapse = ({
  header,
  children,
  active = false,
  className = "",
}) => {
  const StyledCollapse = styled.div`
    ${(props) => {
      const { theme } = props;
      const { Spacing, Breakpoints, Palette, FontSizes, FontWeights } = theme;
      return {
        background: Palette.background.white,
        padding: Spacing[24],
        ".header": {
          cursor: "pointer",
          fontSize: FontSizes.xlText,
          fontWeight: FontWeights.bold,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        },
        ".hdr": {
          flex: 1,
        },
        "icon-container": {
          paddingLeft: Spacing[24],
        },
        [`@media (max-width: ${Breakpoints.md})`]: {
          padding: Spacing[16],
          ".header": {
            fontSize: FontSizes.default,
          },
          "icon-container": {
            paddingLeft: Spacing[16],
          },
        },
      };
    }}
  `;

  const [isCollapsed, setIsCollapsed] = useState(!active);
  return (
    <StyledCollapse className={`${className} collapse`}>
      <div
        className="header"
        onClick={() => {
          setIsCollapsed(!isCollapsed);
        }}
      >
        <div className="hdr">{header}</div>
        <div className="icon-container">{!isCollapsed ? "-" : "+"}</div>
      </div>
      {!isCollapsed && <div className="body">{children}</div>}
    </StyledCollapse>
  );
};
