import {
  Form,
  Button,
  Carousel,
  Col,
  DatePicker,
  Drawer,
  Dropdown,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Spin,
  Timeline,
  Upload,
  Slider,
  Radio,
  message,
  Steps as AntdSteps,
  Checkbox,
  Calendar,
  Badge,
  Popover,
  TimePicker,
  Popconfirm,
  Table,
  Menu,
  Switch,
  Progress,
  Tag,
  Tabs,
  Space,
  Card,
  List,
} from "antd";
import React from "react";

import {
  CopyrightCircleOutlined,
  MenuOutlined,
  UploadOutlined,
  LoadingOutlined,
  CheckCircleOutlined,
  CloseCircleFilled,
  CloseCircleOutlined,
  StarFilled,
  EllipsisOutlined,
  ClockCircleOutlined,
  CaretRightOutlined,
  EditOutlined,
  PlayCircleOutlined,
  InfoCircleOutlined,
  PauseCircleOutlined,
  ExpandOutlined,
  CompressOutlined,
  DownOutlined,
  CloseOutlined,
  StarOutlined,
  ArrowLeftOutlined,
  SearchOutlined,
  LinkOutlined,
  CommentOutlined,
} from "@ant-design/icons";

const FormItemChildren = ({ render, children, ...restProps }) => {
  let childrenArray = React.Children.toArray(children);
  const updatedChildren = childrenArray.map((child) =>
    React.cloneElement(child, restProps)
  );
  return render ? render(restProps) : updatedChildren;
};

const FormItem = ({ children, render, ...restProps }) => {
  return (
    <Form.Item {...restProps}>
      <FormItemChildren render={render}>{children}</FormItemChildren>
    </Form.Item>
  );
};
FormItem.displayName = "Form.Item";

const Icon = ({ type, ...restProps }) => {
  const typeToComponentMap = {
    CopyrightCircleOutlined: CopyrightCircleOutlined,
    MenuOutlined: MenuOutlined,
    UploadOutlined: UploadOutlined,
    LoadingOutlined: LoadingOutlined,
    CloseCircleFilled: CloseCircleFilled,
    StarFilled: StarFilled,
    CloseCircleOutlined: CloseCircleOutlined,
    CheckCircleOutlined: CheckCircleOutlined,
    EllipsisOutlined: EllipsisOutlined,
    ClockCircleOutlined: ClockCircleOutlined,
    CaretRightOutlined: CaretRightOutlined,
    EditOutlined: EditOutlined,
    PlayCircleOutlined: PlayCircleOutlined,
    InfoCircleOutlined: InfoCircleOutlined,
    PauseCircleOutlined: PauseCircleOutlined,
    ExpandOutlined: ExpandOutlined,
    CompressOutlined: CompressOutlined,
    DownOutlined: DownOutlined,
    CloseOutlined: CloseOutlined,
    StarOutlined: StarOutlined,
    ArrowLeftOutlined: ArrowLeftOutlined,
    SearchOutlined: SearchOutlined,
    LinkOutlined: LinkOutlined,
    CommentOutlined: CommentOutlined,
  };

  try {
    const IconOfType = typeToComponentMap[type];
    return <IconOfType {...restProps} />;
  } catch (err) {
    console.error(
      "Icon type not included.. Open components/antd and include your icon"
    );
    return null;
  }
};

export {
  AntdSteps,
  Checkbox,
  Form,
  FormItem,
  Icon,
  Carousel,
  Col,
  DatePicker,
  Drawer,
  Dropdown,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Spin,
  Timeline,
  Upload,
  Slider,
  Radio,
  message,
  Button,
  Calendar,
  Badge,
  Popover,
  TimePicker,
  Popconfirm,
  Table,
  Menu,
  Switch,
  Progress,
  Tag,
  Tabs,
  Space,
  Card,
  List,
};
