import ReactTelephoneImput from "react-telephone-input";
import styled from "styled-components";
import "react-telephone-input/lib/withStyles";

export const TelephoneImput = styled(ReactTelephoneImput)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints } = theme;
    return {
      height: 56,
      boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.08)",
      borderRadius: 6,
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;
