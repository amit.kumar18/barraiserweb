import React from "react";
import styled from "styled-components";

const StyledSteps = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Palette, Spacing } = theme;
    return {
      position: "relative",

      ".horizontal": {
        display: "flex",
        width: "100%",
        paddingBottom: Spacing[24],

        ".step": {
          // justifyContent: "center",

          "&:first-child": {
            // justifyContent: 'flex-start'
          },
          "&:last-child": {
            flex: "none",
            // justifyContent: 'flex-end'
          },
        },
      },

      ".horizontal-line": {
        zIndex: 1,
        position: "absolute",
        width: "100%",
        border: "1px solid #d9d9d9",
        top: "16px",
        left: 0,
        transform: "translateY(-50%)",
      },

      ".step": {
        flex: 1,
        display: "inline-flex",
        zIndex: 2,

        "&.active": {
          ".step-circle": {
            fontWeight: "bold",
            background: Palette.background.yellow,
            borderColor: "transparent",
          },
        },
      },

      ".step-label": {
        position: "relative",

        "&.vertical": {
          ".step-inner": {
            position: "absolute",
            marginTop: Spacing[8],
            display: "flex",
            transform: "translateX(-50%)",
            whiteSpace: "nowrap",
            left: "50%",
          },
          "&.first": {
            ".step-inner": {
              left: 0,
              transform: "none",
            },
          },
          "&.last": {
            ".step-inner": {
              right: 0,
              left: "initial",
              transform: "none",
            },
          },
        },
      },

      ".step-circle": {
        width: "32px",
        height: "32px",
        display: "inline-flex",
        alignItems: "center",
        justifyContent: "center",
        border: `1px solid #d9d9d9`,
        background: "#fff",
        borderRadius: "100%",
      },
    };
  }}
`;

export const Steps = ({
  stepsConfig = [],
  labelPlacement = "vertical",
  direction = "horizontal",
  activeStep,
  showOnlyActiveStepTitle = false,
  className = "",
}) => {
  const isHorizontalSteps = direction === "horizontal";

  return (
    <StyledSteps>
      <div
        className={`${
          isHorizontalSteps ? "horizontal" : "vertical"
        } ${className}`}
      >
        {stepsConfig.map(({ key, title }, index) => {
          const isVerticalLabel = labelPlacement === "vertical";
          const indexClass =
            index === 0
              ? "first"
              : index === stepsConfig.length - 1
              ? "last"
              : "";
          const lpClass = isVerticalLabel
            ? "step-label vertical "
            : "step-label horizontal";

          const isAciveStep = key === activeStep;
          const showTitle = showOnlyActiveStepTitle ? isAciveStep : true;

          return (
            <div className={`step ${isAciveStep ? "active" : ""}`} key={key}>
              <div className={`${lpClass} ${indexClass}`}>
                <div className="step-circle">{index + 1}</div>
                {showTitle && <div className="step-inner">{title}</div>}
              </div>
            </div>
          );
        })}
      </div>
      <div
        className={`${isHorizontalSteps ? "horizontal-line" : "vertical-line"}`}
      />
    </StyledSteps>
  );
};
