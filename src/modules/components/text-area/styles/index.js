import styled from "styled-components";
export const StyledTextArea = styled.textarea`
  ${(props) => {
    return {
      width: "100%",
    };
  }}
`;
