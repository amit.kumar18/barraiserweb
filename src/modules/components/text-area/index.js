import React, { useRef, useState, useEffect } from "react";
import { StyledTextArea } from "./styles";
import autosize from "autosize";

export const BRTextArea = ({ onInput, value, id, placeholder, autoSize }) => {
  const textArea = useRef(null);
  const [selectionStart, setSelectionStart] = useState(0);
  useEffect(() => {
    if (textArea.current) {
      textArea.current.setSelectionRange(selectionStart, selectionStart);
    }
  });
  useEffect(() => {
    autosize(textArea.current);
  }, [value]);
  return (
    <StyledTextArea
      ref={textArea}
      id={id}
      placeholder={placeholder}
      value={value}
      rows={autoSize ? autoSize.minRows : 1}
      onInput={(e) => {
        setSelectionStart(e.target.selectionStart);
        onInput(e);
      }}
      onChange={(e) => null}
    />
  );
};
