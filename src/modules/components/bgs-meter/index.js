import React from "react";
import styled from "styled-components";
import BGSMeterSvg from "assets/images/bgs-meter.svg";
import NeedleSVG from "assets/images/meter-needle.svg";

const StyledMeter = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints } = theme;
    return {
      position: "relative",
      display: "inline-flex",
      ".needle-container": {
        position: "absolute",
        bottom: "8px",
        left: "calc(50% - 12px)",
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export function BGSMeter({ value = 0 }) {
  const rotationVal = (value / 800) * 180 - 166;

  return (
    <StyledMeter>
      <img src={BGSMeterSvg} />
      <span className="needle-container">
        <img
          src={NeedleSVG}
          style={{
            transform: `rotate(${rotationVal}deg)`,
            transformOrigin: "6px 16px",
          }}
        />
      </span>
    </StyledMeter>
  );
}
