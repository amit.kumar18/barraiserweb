import { Button, Container, makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = (width, height) =>
  makeStyles((theme) => ({
    size: {
      width,
      height,
    },
  }));

export function BRButton({ children, width, height, variant, color }) {
  const classes = useStyles(width, height);

  return (
    <Container className={classes.size}>
      <Button fullWidth variant={variant} color={color}>
        {children}
      </Button>
    </Container>
  );
}
