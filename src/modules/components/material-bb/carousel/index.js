import React, { useState } from "react";

import { makeStyles, Grid, MobileStepper, Button } from "@material-ui/core";
import { KeyboardArrowRight, KeyboardArrowLeft } from "@material-ui/icons";
import { BRContainer } from "modules/components";

const useStyles = ({ itemsPerStep }) =>
  makeStyles((theme) => {
    return {
      mobileStepper: {
        width: "20%",
        margin: "auto",
        [theme.breakpoints.down("sm")]: {
          width: "100%",
        },
      },
      gridItem: {
        width: `${100 / itemsPerStep}%`,
        padding: 13,
        [theme.breakpoints.up("xl")]: {
          padding: 20,
        },
      },
    };
  });

export function BRCarousel({ itemsPerStep, items, itemRenderer }) {
  const classes = useStyles({ itemsPerStep })();

  const [activeStep, setActiveStep] = useState(0);

  const displayItems = items.slice(
    activeStep * itemsPerStep,
    activeStep * itemsPerStep + itemsPerStep
  );

  const steps = Math.ceil(items.length / itemsPerStep);

  return (
    <BRContainer maxWidth={false}>
      <Grid container justify="flex-start">
        {displayItems.map((item) => (
          <Grid item className={classes.gridItem}>
            {itemRenderer(item)}
          </Grid>
        ))}
      </Grid>
      {steps > 1 && (
        <MobileStepper
          variant="dots"
          steps={steps}
          position="static"
          activeStep={activeStep}
          className={classes.mobileStepper}
          nextButton={
            <Button
              size="small"
              color="primary"
              onClick={() => setActiveStep(activeStep + 1)}
              disabled={activeStep === steps - 1}
            >
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              color="primary"
              size="small"
              onClick={() => setActiveStep(activeStep - 1)}
              disabled={activeStep === 0}
            >
              <KeyboardArrowLeft />
            </Button>
          }
        />
      )}
    </BRContainer>
  );
}
