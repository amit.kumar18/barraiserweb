import {
  Box,
  Button,
  Container,
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import React from "react";
import { useCMS, CMS_PAGE_NAMES } from "src/cms";

const useStyles = ({ herobg }) =>
  makeStyles((theme) => {
    return {
      root: {
        width: "auto",
        backgroundImage: `url(${herobg})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center 50%",
        padding: "5.55vw 14.1vw 8.82vw",

        textAlign: "start",
        [theme.breakpoints.down("sm")]: {
          height: "auto",
          padding: "53.88vw 5.55vw 13.62vw",
          textAlign: "center",
        },
        [theme.breakpoints.up("xl")]: {
          padding: "5.46vw 18.21vw",
          paddingBottom: 251,
        },
      },
      heroWrapper: {
        width: 721,
        color: theme.palette.primary.contrastText,
        marginLeft: 0,
      },
      heroButtonWrapper: {
        margin: 0,
      },
    };
  });

export const HeroContainer = ({ onClick }) => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const herobg = isMobile
    ? getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["mobile_hero"])
    : getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["laptop_user"]);
  const classes = useStyles({
    herobg,
  })();

  return (
    <Container maxWidth={false} className={classes.root}>
      {isMobile ? (
        <Typography variant="h4" gutterBottom>
          <Box
            color="primary.contrastText"
            textAlign="center"
            dangerouslySetInnerHTML={{
              __html:
                getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["hero_title"]) ||
                "",
            }}
          ></Box>
        </Typography>
      ) : (
        <Typography
          variant="h1"
          className={classes.heroWrapper}
          dangerouslySetInnerHTML={{
            __html:
              getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["hero_title"]) || "",
          }}
          gutterBottom
        />
      )}
      <Container className={classes.heroButtonWrapper}>
        <Button
          color="primary"
          variant="contained"
          size="large"
          onClick={onClick}
        >
          {"Book A Demo"}
        </Button>
      </Container>
    </Container>
  );
};

const useColor = (color) =>
  makeStyles({
    colored: {
      color,
    },
  });

export const Colored = ({ children, color }) => {
  const classes = useColor(color)();
  return <span className={classes.colored}>{children}</span>;
};
