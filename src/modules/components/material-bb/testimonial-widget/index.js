import {
  Box,
  Typography,
  Grid,
  useTheme,
  useMediaQuery,
  Card,
  Avatar,
} from "@material-ui/core";
import React from "react";
import { BRContainer } from "../container";
import { useCMS, CMS_PAGE_NAMES } from "src/cms";
import { useStyles } from "./styles";

export function TestimonialWidget({ testimonials, children }) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isLarge = useMediaQuery(theme.breakpoints.up("xl"));
  const { getContentFromCMS, loadPageContentFromCMS } = useCMS();
  loadPageContentFromCMS(CMS_PAGE_NAMES.COMMON);

  const classes = useStyles();
  return (
    <BRContainer
      maxWidth={false}
      backgroundImage={
        isMobile
          ? ""
          : getContentFromCMS(CMS_PAGE_NAMES.COMMON, ["testimonial_bg"])
      }
      backgroundColor={theme.palette.primary.main}
    >
      <Box
        color="primary.contrastText"
        textAlign="center"
        margin="auto"
        position="relative"
        className={classes.candidateSays}
      >
        <Box
          pt={isMobile ? 22.5 : 26}
          pb={isMobile ? 10 : 0}
          pl={isMobile ? 5.75 : 0}
          pr={isMobile ? 5.5 : 0}
        >
          {children}
        </Box>

        {isMobile ? null : (
          <Box position="absolute" right="0" top={180} width="max-content">
            <img
              src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, ["quote"])}
              className={classes.quoteMark}
            />
          </Box>
        )}

        <Grid
          container
          justify="center"
          className={classes.candidateSaysGrid}
          spacing={8}
        >
          {testimonials &&
            testimonials.map(({ title, para, name, avatar, position }) => (
              <Grid item sm="auto" key={name}>
                <Card className={classes.candidateCard}>
                  <Typography variant="h5" color="inherit">
                    {title}
                  </Typography>

                  <Typography variant="body2" color="textPrimary">
                    {para}
                  </Typography>
                </Card>
                <div className={classes.arrowDown}></div>
                <Box display="flex" justifyContent="center" alignItems="center">
                  <Avatar src={avatar} />
                  <Box marginLeft="1rem" color="white">
                    <Typography variant="subtitle1" color="inherit">
                      {name}
                    </Typography>
                    <Typography variant="body2" color="inherit">
                      {position}
                    </Typography>
                  </Box>
                </Box>
              </Grid>
            ))}
        </Grid>
      </Box>
    </BRContainer>
  );
}
