import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  quoteMark: {
    filter: "brightness(100)",
    transform: "rotate(-180deg)",
    height: 149,
  },
  arrowDown: {
    width: "0",
    height: "0",
    borderLeft: "20px solid transparent",
    borderRight: "20px solid transparent",
    borderTop: "20px solid #fff",
    position: "relative",
    left: "45%",
    marginTop: "-0.5%",
    marginBottom: 20,
  },
  candidateSays: {
    width: "100%",
    paddingBottom: "6.04vw",
    [theme.breakpoints.up("xl")]: {
      paddingBottom: "4.16vw",
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "auto",
      paddingLeft: 23,
      paddingRight: 22,
    },
  },
  candidateSaysGrid: {
    paddingBottom: 19,
    "&>div": {
      paddingBottom: 70,
    },
  },
  candidateCard: {
    width: 450,
    height: 220,
    "& h5": {
      color: theme.palette.grey[900],
      marginBottom: 16,
    },
    [theme.breakpoints.up("xl")]: {
      width: 540,
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "auto",
    },
  },
}));
