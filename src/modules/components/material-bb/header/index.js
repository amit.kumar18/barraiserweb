import React, { useState } from "react";
// import {
//   Link as RouterLink,
//   // useHistory
// } from "react-router-dom";
// import { Link as RouterLink } from "gatsby"
import { Link as RouterLink } from "gatsby-theme-material-ui"

import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Button,
  Toolbar,
  Link,
  Grid,
  Drawer,
  IconButton,
  Divider,
  useMediaQuery,
  useTheme,
  MenuItem,
  Popper,
  Grow,
  Paper,
  ClickAwayListener,
  MenuList,
  useScrollTrigger,
  Collapse,
  Typography,
  Box,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import { Logout } from "modules/components";
import { useSelector } from "react-redux";
import { safeGet } from "src/utils";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& .MuiPaper-rounded": {
      borderRadius: 0,
    },
  },
  logoButton: {
    flexGrow: 1,
    justifyContent: "start",
    cursor: "auto",
    "& img": {
      cursor: "pointer",
      paddingRight: 6.5,
    },
  },
  button: {
    [theme.breakpoints.down("sm")]: {
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      width: "90%",
    },
  },
  mobileLink: {
    margin: "12px 20px",
    fontSize: 16,
    lineHeight: 1.5,
    "& :hover": {
      textDecoration: "none",
    },
  },
  ".noUnderline:hover": {
    textDecoration: "none",
  },
  appBar: {
    background: theme.palette.primary.contrastText,
    borderRadius: 0,
  },
  toolbarMargin: {
    ...theme.mixins.toolbar,
  },
  drawer: {
    "& .MuiDrawer-paperAnchorRight": {
      width: "100%",
      height: "auto",
    },
    "& .MuiDivider-root": {
      marginTop: 28,
    },
  },
  drawerHeader: {
    width: "90%",
    height: 72,
    margin: "auto",
    marginBottom: 20,
  },
  subLinksMenu: {
    padding: 0,
  },
  ".MuiLink-underlineHover:hover": {
    textDecoration: "none",
  },
  sublinkList: {
    backgroundColor: theme.palette.grey[300],
    padding: "3px 0 0 0",
    "& li": {
      paddingTop: 12,
      paddingBottom: 12,
    },
  },
  sublinkStrip: {
    height: 4,
    width: 80,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    backgroundColor: theme.palette.primary.main,
  },
}));

function ElevationScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

export function AppMenuBar() {
  // const history = useHistory();
  const authenticated = useSelector((state) =>
    safeGet(state, ["currentUser", "authenticated"])
  );

  const { getContentFromCMS, loadPageContentFromCMS } = useCMS();
  loadPageContentFromCMS(CMS_PAGE_NAMES.HEADER);
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [openDrawer, setOpenDrawer] = useState(false);

  const navLinks = (
    getContentFromCMS(CMS_PAGE_NAMES.HEADER, ["navigation_links"]) || []
  ).map((x) => JSON.parse(JSON.stringify(x)));
  const navSubLinks = (
    getContentFromCMS(CMS_PAGE_NAMES.HEADER, ["navigation_sub_links"]) || []
  ).map((x) => JSON.parse(JSON.stringify(x)));

  const [anchorEls, setAnchorEls] = React.useState([]);

  const onCloseSubLinksMenu = (idx) => {
    let anchorElsCopy = [...anchorEls];
    anchorElsCopy[idx] = null;
    setAnchorEls(anchorElsCopy);
  };

  navLinks.forEach((navLink) => {
    if (navLink.link.length === 0) {
      navLink.link = null;
    }
    navLink.subLinks = [];
    navSubLinks.forEach((navSubLink) => {
      if (navLink.name === navSubLink.parent) {
        navLink.subLinks.push(navSubLink);
      }
    });
  });

  if (navLinks.length !== anchorEls.length) {
    setAnchorEls(navLinks.map(() => null));
  }

  const authButtons = (
    <>
      {authenticated ? (
        <Logout newButton={true} />
      ) : (
        <Button
          variant="outlined"
          color="primary"
          size="small"
          className={classes.button}
          onClick={() => {
            // history.push("/login");
          }}
        >
          Login
        </Button>
      )}
    </>
  );

  return (
    <>
      <ElevationScroll>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar disableGutters>
            <RouterLink to="/" className={classes.logoButton}>
              <img
                src={getContentFromCMS(CMS_PAGE_NAMES.HEADER, ["brlogo"])}
                alt="BarRaiser Logo"
              />
              {!isMobile && (
                <img
                  src={getContentFromCMS(CMS_PAGE_NAMES.HEADER, ["brname"])}
                />
              )}
            </RouterLink>
            {isMobile ? (
              <>
                <IconButton
                  color="primary"
                  aria-label="menu"
                  edge="end"
                  onClick={() => setOpenDrawer(true)}
                >
                  <MenuIcon />
                </IconButton>
                <Drawer
                  className={classes.drawer}
                  anchor="right"
                  open={openDrawer}
                  onClose={() => setOpenDrawer(false)}
                >
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    className={classes.drawerHeader}
                  >
                    <Grid item>
                      <RouterLink to="/">
                        <img
                          src={getContentFromCMS(CMS_PAGE_NAMES.HEADER, [
                            "brlogo",
                          ])}
                          alt="BarRaiser Logo"
                        />
                      </RouterLink>
                    </Grid>
                    <Grid item>
                      <img
                        src={getContentFromCMS(CMS_PAGE_NAMES.HEADER, [
                          "cancel",
                        ])}
                        onClick={() => setOpenDrawer(false)}
                      />
                    </Grid>
                  </Grid>
                  {navLinks.map((navLink, idx) => (
                    <>
                      {navLink.link ? (
                        <Link
                          variant="button"
                          to={navLink.link}
                          component={RouterLink}
                          className={classes.mobileLink}
                        >
                          {navLink.name}
                        </Link>
                      ) : (
                        <>
                          <Link
                            onClick={(event) => {
                              let anchorElsCopy = [...anchorEls];
                              if (anchorElsCopy[idx] === event.currentTarget) {
                                anchorElsCopy[idx] = null;
                              } else {
                                anchorElsCopy[idx] = event.currentTarget;
                              }
                              setAnchorEls(anchorElsCopy);
                            }}
                            className={classes.mobileLink}
                          >
                            {navLink.name}
                          </Link>
                          <MenuList
                            autoFocusItem={Boolean(anchorEls[idx])}
                            className={classes.subLinksMenu}
                          >
                            {navLink.subLinks.map((subLink) => (
                              <Collapse in={Boolean(anchorEls[idx])}>
                                <MenuItem>
                                  <Link
                                    variant="button"
                                    to={subLink.link}
                                    component={RouterLink}
                                  >
                                    {subLink.name}
                                  </Link>
                                </MenuItem>
                              </Collapse>
                            ))}
                          </MenuList>
                        </>
                      )}
                    </>
                  ))}

                  {/* {authButtons} */}
                  <Divider />
                </Drawer>
              </>
            ) : (
              <>
                <Grid>
                  {navLinks.map((navLink, idx) => (
                    <>
                      {navLink.link ? (
                        <Link
                          variant="button"
                          to={navLink.link}
                          component={RouterLink}
                          className={classes.noUnderline}
                        >
                          {navLink.name}
                        </Link>
                      ) : (
                        <>
                          <Link
                            variant="button"
                            className={classes.noUnderline}
                            onClick={(event) => {
                              let anchorElsCopy = [...anchorEls];
                              anchorElsCopy[idx] = event.currentTarget;
                              setAnchorEls(anchorElsCopy);
                            }}
                          >
                            {navLink.name}
                          </Link>
                          <Popper
                            open={Boolean(anchorEls[idx])}
                            anchorEl={anchorEls[idx]}
                            transition
                            disablePortal
                            placement="bottom-start"
                            modifiers={{
                              offset: {
                                enabled: true,
                                offset: "0, 24",
                              },
                            }}
                          >
                            {({ TransitionProps, placement }) => (
                              <Grow
                                {...TransitionProps}
                                style={{
                                  transformOrigin:
                                    placement === "bottom"
                                      ? "center top"
                                      : "center bottom",
                                }}
                              >
                                <Paper>
                                  <Box className={classes.sublinkStrip}></Box>
                                  <ClickAwayListener
                                    onClickAway={() => onCloseSubLinksMenu(idx)}
                                  >
                                    <MenuList
                                      disablePadding
                                      className={classes.sublinkList}
                                    >
                                      {navLink.subLinks.map((subLink) => (
                                        <MenuItem>
                                          <Link
                                            variant="button"
                                            to={subLink.link}
                                            component={RouterLink}
                                          >
                                            <Typography
                                              variant="button"
                                              color="textPrimary"
                                            >
                                              {subLink.name}
                                            </Typography>
                                          </Link>
                                        </MenuItem>
                                      ))}
                                    </MenuList>
                                  </ClickAwayListener>
                                </Paper>
                              </Grow>
                            )}
                          </Popper>
                        </>
                      )}
                    </>
                  ))}
                </Grid>
                {/* {authButtons} */}
              </>
            )}
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <div className={classes.toolbarMargin} />
    </>
  );
}
