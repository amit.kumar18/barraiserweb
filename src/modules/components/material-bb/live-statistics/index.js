import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Grid, Box, Typography, useMediaQuery } from "@material-ui/core";
import { useCMS, CMS_PAGE_NAMES } from "src/cms";

const useStyles = makeStyles((theme) => ({
  parentGrid: {
    width: 1038,

    padding: "37px 0",
    margin: "auto",
    [theme.breakpoints.down("md")]: {
      width: "auto",
      padding: "16.82vw 0 23.2vw",
    },
    [theme.breakpoints.up("xl")]: {
      padding: "45px 0",
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: "17.77vw",
      paddingBottom: "25.83vw",
    },
  },
  icon: {
    width: 40,
  },
}));

const labels = {
  numberOfPartners: "Businesses Onboard",
  numberOfInterviews: "Candidates Interviewed",
  numberOfExperts: "Expert Partnered",
};
const icons = {
  numberOfInterviews: "candidate",
  numberOfExperts: "expert",
  numberOfPartners: "business",
};

export function LiveStatistics({ data }) {
  const classes = useStyles();
  return (
    <Box bgcolor="#F9F9F9">
      <Grid container className={classes.parentGrid} justify="center">
        {data &&
          Object.keys(labels).map((key) => (
            <Grid item xs={10} sm={4}>
              <StatisticItem
                icon={icons[key]}
                number={data[key]}
                subtitle={labels[key]}
              />
            </Grid>
          ))}
      </Grid>
    </Box>
  );
}

function StatisticItem({ icon, number, subtitle }) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const { getContentFromCMS } = useCMS();
  const classes = useStyles();
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      pt={isMobile ? "8.33vw" : 3}
    >
      <img
        src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [`${icon}_icon`])}
        className={classes.icon}
      />

      <Box pb={1}>
        <Typography variant="h3" color="primary">
          {number ? `${number}+` : ""}
        </Typography>
      </Box>

      <Typography variant="subtitle1">
        <Box color="#A0A0A0">{subtitle}</Box>
      </Typography>
    </Box>
  );
}
