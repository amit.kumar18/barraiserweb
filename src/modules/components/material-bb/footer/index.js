import React, { useState } from "react";
import { Link as RouterLink } from "gatsby-theme-material-ui";
import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  useMediaQuery,
  useTheme,
  Typography,
  Link,
  TextField,
  Button,
  Box,
  Dialog,
} from "@material-ui/core";
import {
  BRContainer,
  BOOK_DEMO_CONTEXT_TYPE,
} from "modules/components";

import { useCMS, CMS_PAGE_NAMES } from "src/cms";

import { useMutation } from "@apollo/react-hooks";
import { BOOK_DEMO } from "modules/components/material-bb/book-demo/graphql";

import { isValidEmail } from "src/utils";

const useStyles = (isMobile) =>
  makeStyles((theme) => ({
    root: {
      textAlign: isMobile ? "start" : "auto",
      padding: "5.7vw 2.85vw 7.36vw 1.975vw",
      [theme.breakpoints.down("sm")]: {
        padding: "91px 24px 0",
      },
      [theme.breakpoints.up("xl")]: {
        padding: "4.16vw 8.33vw 5.46vw 9.63vw",
      },
    },
    logo: {
      width: "11.11vw",
      marginBottom: "1.32vw",
      [theme.breakpoints.down("sm")]: {
        width: "45%",
        marginBottom: 19,
      },
    },
    logoContainer: {
      textAlign: "left",
      "& h6": {
        width: "22.43vw",
        color: theme.palette.grey[700],
        maxWidth: 310,
        [theme.breakpoints.down("sm")]: {
          width: "auto",
        },
      },
    },
    allLinksContainer: {
      "& p, a": {
        margin: 0,
        marginBottom: 19,
        display: "block",
        [theme.breakpoints.down("sm")]: {
          marginBottom: 19,
        },
      },
      "& a": {
        color: theme.palette.grey[700],
        textTransform: "none",
      },
      [theme.breakpoints.down("sm")]: {
        padding: "50px 0px",
        paddingBottom: 0,
        "& > div": {
          paddingBottom: "16.94vw",
        },
      },
    },

    earlyAccessContainer: {
      "& > h6": {
        color: theme.palette.grey[700],
        marginBottom: 32,
        [theme.breakpoints.down("sm")]: {
          float: "none",
        },
      },
      "& button": {
        borderRadius: "6px",
      },
    },
    textField: {
      "& div": {
        borderRadius: isMobile ? "6px" : "6px 0 0 6px",
        height: 56,
        width: isMobile ? "auto" : 255,
        marginBottom: 5,
      },
    },
    copyrightContainer: {
      "& span": {
        color: theme.palette.grey[700],
        margin: "0 20px",
      },
      textAlign: "center",
      marginTop: "5.76vw",
      [theme.breakpoints.down("sm")]: {
        marginTop: 24,
        marginBottom: 2,
      },
      [theme.breakpoints.up("xl")]: {
        marginTop: "4.4vw",
      },
    },
    emailBtn: {
      width: 128,
      height: 56,
      borderRadius: isMobile ? "6px" : "0 6px 6px 0 !important",
      [theme.breakpoints.down("sm")]: {
        width: "auto",
        position: "relative",
        right: 15,
      },
    },
    labelAsterisk: {
      color: "#DA1414",
    },
    labelEmailInput: {
      color: theme.palette.grey[900],
    },
    notchedOutline: {
      borderColor: "#6EC2D4 !important",
    },
  }));

export function BRFooter() {
  const { loadPageContentFromCMS, getContentFromCMS } = useCMS();
  loadPageContentFromCMS(CMS_PAGE_NAMES.FOOTER);
  loadPageContentFromCMS(CMS_PAGE_NAMES.COMMON);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles(isMobile)();

  const [email, setEmail] = useState("");
  const [openFormModal, setOpenFormModal] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [submitClicked, setSubmitClicked] = useState(false);

  const [bookDemo] = useMutation(BOOK_DEMO, {
    onCompleted: ({ registerEnquiry }) => {
      const { message } = registerEnquiry;
      setSuccessMessage(message);
      setOpenFormModal(true);
    },
    onError: (err) => {
      setOpenFormModal(false);
      setErrorMessage(
        "There is problem processing your query, please come back later"
      );
    },
  });

  function registerAnEnquiry() {
    bookDemo({
      variables: {
        input: {
          topic: BOOK_DEMO_CONTEXT_TYPE.EARLY_ACCESS,
          email,
        },
      },
    });
  }

  const btnlabel = (
    <Typography variant="caption" className={classes.labelEmailInput}>
      Email Address<sup className={classes.labelAsterisk}>*</sup>
    </Typography>
  );

  return (
    <BRContainer
      maxWidth={false}
      className={classes.root}
      backgroundColor={theme.palette.common.white}
    >
      <Grid container justify="space-between">
        <Grid item md="auto" xs={12} className={classes.logoContainer}>
          <img
            className={classes.logo}
            src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, ["brlogo"])}
          />
          <Typography variant="subtitle1">
            {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["slogan"])}
          </Typography>
        </Grid>
        <Grid
          item
          container
          md={5}
          xs={12}
          className={classes.allLinksContainer}
        >
          <Grid item md={5} xs={12}>
            <Typography variant="body1" color="primary">
              {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["link_group_1_title"])}
            </Typography>

            {(
              getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["link_group_1"]) || []
            ).map((item) => (
              <Link
                variant="body1"
                component={RouterLink}
                to={item.link}
                key={item.name}
              >
                {item.name}
              </Link>
            ))}
          </Grid>
          <Grid item md={5} xs={12}>
            <Typography variant="body1" color="primary">
              {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["link_group_2_title"])}
            </Typography>

            {(
              getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["link_group_2"]) || []
            ).map((item) => (
              <Link
                variant="body1"
                component={RouterLink}
                to={item.link}
                key={item.name}
              >
                {item.name}
              </Link>
            ))}
          </Grid>
        </Grid>
        <Grid item md={"auto"} xs={12} className={classes.earlyAccessContainer}>
          <Typography variant={isMobile ? "h6" : "subtitle1"}>
            {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["early_access_text"])}
          </Typography>
          <Grid container justify={isMobile ? "center" : "flex-end"}>
            <TextField
              label={btnlabel}
              variant="outlined"
              className={classes.textField}
              InputLabelProps={{ shrink: true }}
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
              }}
              placeholder="jane@example.com"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              error={!isValidEmail(email) && submitClicked}
              helperText={
                !isValidEmail(email) &&
                submitClicked &&
                "Please enter a valid email address"
              }
            />
            <Button
              variant="contained"
              color="primary"
              className={classes.emailBtn}
              onClick={() => {
                setSubmitClicked(true);
                if (isValidEmail(email)) {
                  registerAnEnquiry();
                }
              }}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {isMobile ? (
        <>
          <BRContainer className={classes.copyrightContainer}>
            <Typography variant="caption" display="inline" align="center">
              Privacy policy
            </Typography>
            {/* <Typography variant="caption" display="inline" align="center">
              Sitemap
            </Typography> */}
          </BRContainer>
          <BRContainer className={classes.copyrightContainer}>
            <Typography variant="caption" align="center">
              {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["copyright"])}
            </Typography>
          </BRContainer>
        </>
      ) : (
        <BRContainer className={classes.copyrightContainer}>
          {/* <Typography variant="caption" display="inline" align="center">
            Sitemap
          </Typography> */}
          <Typography variant="caption" display="inline" align="center">
            {getContentFromCMS(CMS_PAGE_NAMES.FOOTER, ["copyright"])}
          </Typography>
        </BRContainer>
      )}
      {successMessage && openFormModal && (
        <Dialog
          aria-labelledby="modal-title"
          modal={true}
          open={openFormModal}
          onClose={() => setOpenFormModal(false)}
        >
          <Box p={10} pt={20} pb={20} width={isMobile ? "auto" : 600}>
            <Box textAlign="center">
              <Typography variant="h3" color="primary">
                Thank you
              </Typography>
              <Typography variant="h4">
                <Box fontWeight={500} color="#414141" mt={2} mb={2}>
                  for submitting your details.
                </Box>
              </Typography>
              <Typography variant="body1" color="#414141">
                <Box fontWeight={400} mb={2}>
                  Our team will connect with you shortly.
                </Box>
              </Typography>
              <Button
                variant="contained"
                color="primary"
                onClick={() => setOpenFormModal(false)}
                size="small"
              >
                Close
              </Button>
            </Box>
          </Box>
        </Dialog>
      )}
      <Dialog
        aria-labelledby="modal-title"
        modal={true}
        open={!!errorMessage}
        onClose={() => setErrorMessage("")}
      >
        <Typography variant="h5">
          <Box pt={4} textAlign="center">
            Error
          </Box>
        </Typography>
        <Box p={8} color={theme.palette.error.main}>
          {errorMessage}
        </Box>
        <Box margin="auto">
          <Button
            variant="contained"
            color="primary"
            onClick={() => setErrorMessage("")}
            size="small"
          >
            close
          </Button>
        </Box>
      </Dialog>
    </BRContainer>
  );
}
