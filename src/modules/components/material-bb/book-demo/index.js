import React from "react";
import {
  Grid,
  TextField,
  MenuItem,
  Button,
  Box,
  makeStyles,
} from "@material-ui/core";
import { BOOK_DEMO } from "./graphql";
import { useMutation } from "@apollo/react-hooks";
import { isValidEmail, isValidPhone } from "src/utils";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiOutlinedInput-root": {
      boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.08)",
      borderRadius: 6,
    },
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#fff",
    },
  },
  formTextLabel: {
    ...theme.mixins.formTextLabel,
  },
}));

export function BookADemoForm({
  onSuccess,
  onError,
  submitButtonText,
  context,
  ...fields
}) {
  const [val, setVal] = React.useState({
    name: "",
    topic: "",
    phone: "",
    email: "",
    message: "",
    organisation: "",
  });
  const [errors, setErrors] = React.useState({
    name: "",
    topic: "",
    phone: "",
    email: "",
    message: "",
    organisation: "",
  });

  const classes = useStyles();

  const [bookDemo, { error }] = useMutation(BOOK_DEMO, {
    onCompleted: ({ registerEnquiry }) => {
      const { message } = registerEnquiry;
      onSuccess(message);
    },
    onError: (err) => {
      onError("There is problem processing your query, please come back later");
    },
  });

  function registerAnEnquiry(values) {
    if (context) {
      values.topic = context;
    }
    bookDemo({
      variables: {
        input: values,
      },
    });
  }

  function validateField(name, value, newErrors) {
    if (fields[name].optional && !value) {
      return true;
    }
    switch (name) {
      case "topic": {
        if (!value) {
          newErrors[name] = "Please choose a valid option";
          return false;
        } else {
          newErrors[name] = "";
        }
        break;
      }
      case "phone": {
        if (!isValidPhone(value)) {
          newErrors[name] = "Please enter a valid phone number";
          return false;
        } else {
          newErrors[name] = "";
        }
        break;
      }

      case "email": {
        if (!isValidEmail(value)) {
          newErrors[name] = "Please enter a valid email address";
          return false;
        } else {
          newErrors[name] = "";
        }
        break;
      }
      default: {
        if (!value) {
          newErrors[name] = "It can't be empty";
          return false;
        } else {
          newErrors[name] = "";
        }
        break;
      }
    }
    return true;
  }

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setVal({ ...val, [name]: value });
  };

  const handleSubmit = () => {
    let isErrors = false;
    const newErrors = { ...errors };

    Object.keys(fields).forEach((key) => {
      if (!validateField(key, val[key], newErrors)) {
        isErrors = true;
      }
    });

    setErrors(newErrors);
    if (!isErrors) {
      const payload = {};
      Object.keys(fields).forEach((field) => {
        payload[field] = val[field];
      });
      registerAnEnquiry(payload);
      return;
    }
  };

  return (
    <Grid
      container
      direction={"column"}
      justify="center"
      spacing={6}
      m={4}
      className={classes.root}
    >
      {fields.name && (
        <Grid item>
          <TextField
            name="name"
            label="Name"
            variant="outlined"
            fullWidth
            autoFocus
            value={val.name}
            onChange={handleChange}
            helperText={errors.name}
            error={!!errors.name}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          />
        </Grid>
      )}
      {fields.topic && (
        <Grid item>
          <TextField
            name="topic"
            label="I am (Select one)"
            variant="outlined"
            fullWidth
            value={val.topic}
            onChange={handleChange}
            displayEmpty
            select
            helperText={errors.topic}
            error={!!errors.topic}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          >
            <MenuItem value="">None</MenuItem>
            <MenuItem value="expert">Domain Expert</MenuItem>
            <MenuItem value="candidate">Job Candidate</MenuItem>
            <MenuItem value="company">Hiring Company</MenuItem>
          </TextField>
        </Grid>
      )}
      {fields.phone && (
        <Grid item>
          <TextField
            name="phone"
            type="tel"
            label="Phone number"
            variant="outlined"
            fullWidth
            value={val.phone}
            onChange={handleChange}
            helperText={errors.phone}
            error={!!errors.phone}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          />
        </Grid>
      )}
      {fields.email && (
        <Grid item>
          <TextField
            name="email"
            type="email"
            label="Email"
            variant="outlined"
            fullWidth
            value={val.email}
            onChange={handleChange}
            helperText={errors.email}
            error={!!errors.email}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          />
        </Grid>
      )}
      {fields.organisation && (
        <Grid item>
          <TextField
            name="organisation"
            type="text"
            label="Organization"
            variant="outlined"
            fullWidth
            value={val.organisation}
            onChange={handleChange}
            helperText={errors.organisation}
            error={!!errors.organisation}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          />
        </Grid>
      )}
      {fields.message && (
        <Grid item>
          <TextField
            name="message"
            id="outlined-multiline-flexible"
            label="Message"
            placeholder="Write a message"
            multiline
            variant="outlined"
            fullWidth
            rows={2}
            rowsMax={6}
            value={val.message}
            onChange={handleChange}
            helperText={errors.message}
            error={!!errors.message}
            InputLabelProps={{
              classes: {
                root: classes.formTextLabel,
              },
            }}
          />
        </Grid>
      )}
      <Grid item>
        <Box mt={4}>
          <Button
            variant="contained"
            color="primary"
            size="large"
            fullWidth
            onClick={handleSubmit}
          >
            {!val.topic && submitButtonText}
            {val.topic === "expert" && "Become an expert"}
            {val.topic === "candidate" && "Get your score"}
            {val.topic === "company" && "Book a demo"}
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
}

export * from "./constants";
