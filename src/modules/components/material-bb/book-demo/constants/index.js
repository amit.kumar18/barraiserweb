export const BOOK_DEMO_CONTEXT_TYPE = {
  BOOK_A_DEMO: "book_a_demo",
  BECOME_AN_EXPERT: "become_an_expert",
  EARLY_ACCESS: "early_access",
  CANDIDATE: "candidate",
};
