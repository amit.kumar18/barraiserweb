import gql from "graphql-tag";

export const BOOK_DEMO = gql`
  mutation registerEnquiry($input: RegisterEnquiryInput) {
    registerEnquiry(input: $input) {
      message
    }
  }
`;
