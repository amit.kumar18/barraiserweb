import React from "react";

import { Container, makeStyles } from "@material-ui/core";

const useStyles = ({
  backgroundImage,
  backgroundColor,
  width,
  height,
  minHeight,
  backgroundSize,
  backgroundPosition,
  bgGradient,
}) =>
  makeStyles(() => {
    return {
      root: {
        backgroundImage: bgGradient
          ? ` ${bgGradient}, url(${backgroundImage})`
          : `url(${backgroundImage})`,
        backgroundColor: backgroundColor,
        backgroundSize: backgroundSize || "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: backgroundPosition || "center",
        width: width ? width : "auto",
        height: height,
        minHeight: minHeight,
      },
    };
  });

export function BRContainer({
  children,
  backgroundImage,
  backgroundColor,
  width,
  height,
  minHeight,
  className,
  backgroundSize,
  backgroundPosition,
  bgGradient,
  ...muiContainerProps
}) {
  const classes = useStyles({
    backgroundImage,
    backgroundColor,
    width,
    height,
    minHeight,
    backgroundSize,
    backgroundPosition,
    bgGradient,
  })();
  return (
    <Container
      {...muiContainerProps}
      className={`${classes.root} ${className}`}
    >
      {children}
    </Container>
  );
}
