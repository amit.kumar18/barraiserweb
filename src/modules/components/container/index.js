import styled from "styled-components";

export const StyledDiv = styled.div`
  ${(props) => {
    return {
      justifyContent: "center",
      marginLeft: ".5rem",
      textAlign: "center",
    };
  }}
`;

export const BorderedContainer = styled.div`
  ${(props) => {
    return {
      border: "solid 1px black",
    };
  }}
`;
