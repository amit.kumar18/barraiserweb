import React from "react";
import styled from "styled-components";

import { Seperator } from "modules/components";

const StyledHeroSection = styled.div`
  ${(props) => {
    const {
      theme,
      styles = {},
      backgroundImage,
      mobileBackgroundImage = "",
    } = props;

    const { Palette, Breakpoints, Spacing } = theme;
    const { padding, ...restStyles } = styles;
    return {
      ...restStyles,
      padding: padding ? padding : Spacing[40],
      background: `linear-gradient(45deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(${backgroundImage})`,
      backgroundPosition: "center",
      backgroundSize: "cover",
      color: Palette.text.white,
      textAlign: "center",
      [`@media (max-width: ${Breakpoints.md})`]: {
        padding: `${Spacing[16]}`,
        backgroundPosition: "left",
        ...(mobileBackgroundImage && {
          background: `linear-gradient(45deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(${mobileBackgroundImage})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
        }),
      },
    };
  }}
`;

const StyledHeroText = styled.div`
  ${(props) => {
    const { theme } = props;
    const { FontSizes, Breakpoints } = theme;
    return {
      fontSize: FontSizes.h1,
      maxWidth: 800,
      margin: "0 auto",
      [`@media (max-width: ${Breakpoints.md})`]: {
        fontSize: FontSizes.xlText,
      },
    };
  }}
`;

const StyledSeperatorContainer = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints } = theme;
    return {
      margin: `${Spacing[32]} 0`,
      [`@media (max-width: ${Breakpoints.md})`]: {
        margin: `${Spacing[24]} 0`,
      },
    };
  }}
`;

const StyledSubtext = styled.div`
  ${(props) => {
    const { theme } = props;
    const { FontSizes, FontWeights, Breakpoints } = theme;
    return {
      fontSize: FontSizes.h3,
      fontWeight: FontWeights.semibold,
      maxWidth: 800,
      margin: "0 auto",
      [`@media (max-width: ${Breakpoints.md})`]: {
        fontSize: FontSizes.mediumText,
      },
    };
  }}
`;

const StyledExtraContent = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints } = theme;
    return {
      marginTop: Spacing[32],
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export function HeroSection({
  backgroundImage,
  heading,
  subHeading,
  extraContent,
  styles,
  className,
  mobileBackgroundImage,
}) {
  return (
    <StyledHeroSection
      className={className}
      backgroundImage={backgroundImage}
      mobileBackgroundImage={mobileBackgroundImage}
      styles={styles}
    >
      <StyledHeroText>{heading}</StyledHeroText>
      <StyledSeperatorContainer>
        <Seperator />
      </StyledSeperatorContainer>
      <StyledSubtext>{subHeading}</StyledSubtext>

      {extraContent && <StyledExtraContent>{extraContent}</StyledExtraContent>}
    </StyledHeroSection>
  );
}
