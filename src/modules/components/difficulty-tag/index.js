import React from "react";
import styled from "styled-components";
import { Tag } from "components";
import { DIFFICULTY_COLOR } from "modules/constants";

const StyledDifficultyTag = styled(`div`)`
  ${(props) => {
    const { theme, isFollowUp, difficulty } = props;
    const { Spacing, FontWeights, Palette } = theme;
    const difficultyColor = DIFFICULTY_COLOR[difficulty];
    const backgroundColor = isFollowUp
      ? Palette.background.white
      : difficultyColor;
    const fontColor = isFollowUp ? difficultyColor : Palette.background.white;
    const borderColor = isFollowUp ? difficultyColor : Palette.background.none;
    return {
      ".difficulty-tag": {
        borderRadius: "100px",
        fontWeight: FontWeights.bold,
        color: fontColor,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: "2px",
        textAlign: "center",
        padding: `${Spacing[4]} ${Spacing[12]}`,
        margin: "0px !important",
      },
      ".difficulty-div": {
        fontSize: "9px",
        fontWeight: "400",
        lineHeight: "normal",
        textAlign: "center",
      },
    };
  }}
`;

export const DifficultyTag = (props) => {
  return (
    <StyledDifficultyTag {...props}>
      <Tag className="difficulty-tag">{props.children}</Tag>
      <br />
      <div className="difficulty-div">DIFFICULTY</div>
    </StyledDifficultyTag>
  );
};
