import gql from "graphql-tag";

export const FETCH_USER_TOUR_HISTORY = gql`
  query GetUserTourHistory {
    getUserTourHistory
  }
`;

export const SAVE_TOUR_HISTORY = gql`
  mutation SaveUserTourHistory($input: UserTourHistoryInput!) {
    saveUserTourHistory(input: $input)
  }
`;