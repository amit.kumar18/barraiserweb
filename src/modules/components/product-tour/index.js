import Joyride, {ACTIONS, EVENTS, STATUS} from "react-joyride";
import React, {useEffect, useState, forwardRef, useImperativeHandle} from "react";
import {useLazyQuery, useMutation} from "@apollo/react-hooks";
import {message} from "components";
import {FETCH_USER_TOUR_HISTORY, SAVE_TOUR_HISTORY} from "./graphql";

export const ProductTour = forwardRef( ({steps, page, ...props}, ref)=> {
  const [stepIndex, setStepIndex] = useState(null);
  const [run, setRun] = useState(true);
  const [tourSteps, setTourSteps] = useState([]);
  const [fetchedTourHistory, setFetchedTourHistory] = useState({});

  const getNewSteps = (userTourHistory, steps) => {
    if (!userTourHistory || !userTourHistory[page]) {
      return addDefaultStepConfig(steps);
    }
    return addDefaultStepConfig(steps.filter(step => {
      return !userTourHistory[page].includes(step.id)
    }));
  }

  const addDefaultStepConfig = (steps) => {
    return steps.map(step => {
      step =  {
        disableBeacon: true,
        spotlightClicks: true,
        ...step
      }
      if(step.mandatoryStep === true){
        step = {
          ...step,
          hideFooter: true,
          disableOverlayClose: true,
          hideCloseButton: true,
        }
      }
      return step
    })
  }

  const [
    fetchUserTourHistoryDataAPI,
    {loading: fetchingUserHistory},
  ] = useLazyQuery(FETCH_USER_TOUR_HISTORY, {
    onCompleted: (apiRespData) => {
      if(apiRespData){
        setFetchedTourHistory(JSON.parse(apiRespData.getUserTourHistory));
        setTourSteps(getNewSteps(JSON.parse(apiRespData.getUserTourHistory), steps));
      } else {
        setFetchedTourHistory(null);
        setTourSteps(getNewSteps([], steps));
      }
    },
    onError: () => {

    },
  });

  useImperativeHandle(ref, () => ({
    gotoNextStep() {
      setStepIndex(stepIndex+1);
    }
  }));

  useEffect(() => {
    fetchUserTourHistoryDataAPI();
  }, [])

  const [saveTourHistory, {loading: saving}] = useMutation(
    SAVE_TOUR_HISTORY,
    {
      onCompleted: ({apiResponse}) => {

      },
      onError: () => {

      },
    }
  );

  const saveHistory = () => {

    if (tourSteps.length === 0) return

    let payload = JSON.parse(JSON.stringify(fetchedTourHistory));

    if (!payload) payload = {};
    if (!payload[page]) payload[page] = [];

    payload[page] = payload[page].concat(tourSteps.map(step => step.id))

    saveTourHistory({
      variables: {
        input: {
          history: JSON.stringify(payload),
        },
      },
    });
  };

  const handleJoyrideCallback = (data) => {
    let {action, index, status, type, step} = data;

    if ([EVENTS.STEP_AFTER].includes(type)) {
      // Update state to advance the tour
      setStepIndex(index + (action === ACTIONS.PREV ? -1 : 1))
    } else if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
      // Need to set our running state to false, so we can restart if we click start again.
      // this.setState({ run: false });
      setStepIndex(0);
      setRun(false);
      saveHistory();
    }
  }

  return (
    <Joyride
      steps={tourSteps}
      run={run}
      stepIndex={stepIndex}
      callback={handleJoyrideCallback}
      continuous={true}
      scrollOffset={200}
      showSkipButton={true}
      styles={{
        options: {
          arrowColor: '#ffffff',
          backgroundColor: '#ffffff',
          primaryColor: '#40a9ff',
          textColor: '#000',
          zIndex: 1000,
        }
      }}
    />
  )
})

