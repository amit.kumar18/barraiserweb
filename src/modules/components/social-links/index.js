import React from "react";
import styled from "styled-components";

import FacebookPng from "assets/images/facebook.png";
import LinkedinPng from "assets/images/linkedin.png";
import TwitterPng from "assets/images/twitter.png";
import InstaPng from "assets/images/insta.png";

const StyledSocialLinks = styled.div`
  ${props => {
    const { theme } = props;
    const { Breakpoints, Spacing, FontSizes } = theme;
    return {
      display: "inline-flex",
      flexDirection: "column",
      ".hdr": {
        fontSize: FontSizes.mobileDefault,
        marginBottom: Spacing[12]
      },
      ".links": {
        a: {
          marginRight: Spacing[16],
          "&:last-child": {
            marginRight: Spacing[0]
          }
        }
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        ".hdr": {
          fontSize: FontSizes.mobileDefault,
          marginBottom: Spacing[12]
        },
        ".links": {}
      }
    };
  }}
`;

export function SocialLinks(props) {
  const { className = "" } = props;
  /*eslint-disable */
  return (
    <StyledSocialLinks className={className}>
      <div className="hdr">Follow us on social media</div>
      <div className="links">
        <a href="https://www.facebook.com/raiser.bar" target="_blank">
          <img src={FacebookPng} alt="" />
        </a>
        <a href="https://twitter.com/raiser_bar" target="_blank">
          <img src={TwitterPng} alt="" />
        </a>
        <a href="https://www.linkedin.com/company/raiserbar" target="_blank">
          <img src={LinkedinPng} alt="" />
        </a>
        <a href="https://www.instagram.com/raiser.bar" target="_blank">
          <img src={InstaPng} alt="" />
        </a>
      </div>
    </StyledSocialLinks>
  );
  /*eslint-enable */
}
