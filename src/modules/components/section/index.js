import React from "react";
import { Row, Col } from "components";
import { Seperator } from "modules/components";

import styled from "styled-components";

export const StyledRow = styled(Row)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Spacing } = theme;
    return {
      paddingLeft: `${Spacing[32]} !important`,
      paddingRight: `${Spacing[32]} !important`,
      width: "100%",
      [`@media (max-width: ${Breakpoints.md})`]: {
        paddingLeft: `${Spacing[12]} !important`,
        paddingRight: `${Spacing[12]} !important`,
      },
    };
  }}
`;
export function SectionBase(props) {
  const { children, ...restProps } = props;
  return (
    <StyledRow {...restProps}>
      <Col md={0} lg={3} />
      <Col xs={24} sm={24} md={24} lg={18}>
        {children}
      </Col>
      <Col md={0} lg={3} />
    </StyledRow>
  );
}

export const Section = styled(SectionBase)`
  ${(props) => {
    const { theme } = props;
    const { Spacing } = theme;
    return {
      padding: `${Spacing[40]} 0`,
    };
  }}
`;

const StyledSectionHeader = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, FontWeights, FontSizes } = theme;
    return {
      fontSize: FontSizes.h2,
      fontWeight: FontWeights.semibold,
      textAlign: "center",
      lineHeight: 1,
      [`@media (max-width: ${Breakpoints.md})`]: {
        fontSize: FontSizes.xlText,
        fontWeight: FontWeights.bold,
      },
    };
  }}
`;

const StyledSeparator = styled(Seperator)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Spacing } = theme;
    return {
      marginTop: Spacing[32],
      marginBottom: Spacing[32],
      [`@media (max-width: ${Breakpoints.md})`]: {
        marginTop: Spacing[24],
        marginBottom: Spacing[24],
      },
    };
  }}
`;

export const SectionHeader = function (props) {
  return (
    <div>
      <StyledSectionHeader>{props.children}</StyledSectionHeader>
      <StyledSeparator />
    </div>
  );
};
