import styled from "styled-components";

const StyledNavItem = styled.span`
  ${props => {
    const { theme } = props;
    const { Palette, FontWeights } = theme;
    return {
      display: "inline-flex",
      color: Palette.text.default,
      fontWeight: props.active ? FontWeights.bold : FontWeights.default,
      position: "relative",
      "&:after": {
        position: "absolute",
        content: "''",
        top: "100%",
        left: 0,
        height: "2px",
        width: "100%",
        background: props.active
          ? Palette.background.yellow
          : Palette.background.none
      }
    };
  }}
`;

export const NavItem = StyledNavItem;
