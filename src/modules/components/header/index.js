import React, { useState } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import {
  Row,
  Col,
  Icon,
  Drawer,
  BrandLogo,
  LineButton,
  Flex,
} from "components";
import styled from "styled-components";

import { SocialLinks, Logout } from "modules/components";
import { ROUTE_PREFIXES } from "constantVars";
import { NavItem } from "./nav-item";

export const StyledHeader = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Palette, Spacing } = theme;
    return {
      background: Palette.background.white,
      padding: `${Spacing[12]} ${Spacing[32]}`,
      ".links": {
        "&>*": {
          paddingLeft: Spacing[40],
        },
        "&>:first-child": {
          paddingLeft: Spacing[0],
        },
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        padding: Spacing[12],
      },
    };
  }}
`;

const StyledHeaderButtonsSection = styled(Col)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints } = theme;
    return {
      ".mr": {
        marginRight: 12,
      },
      button: {
        display: "inline-flex",
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StyledDrawerRow = styled(Row)`
  ${(props) => {
    const { theme } = props;
    const { Spacing } = theme;
    return {
      display: "flex",
      flexDirection: "column",
      [NavItem]: {
        marginBottom: Spacing[32],
      },
      ".separator": {
        border: "1px solid #ececec",
        marginBottom: Spacing[32],
      },
      ".button": {
        display: "flex",
        marginBottom: Spacing[32],
      },
    };
  }}
`;

const StyledRow = styled(Row)`
  ${(props) => ({
    display: "flex",
    alignItems: "center",
  })}
`;

export function PageHeader() {
  const [isDrawerOpen, setDrawerOpenState] = useState(false);
  const fnToggleDrawerOpenState = () => {
    setDrawerOpenState(!isDrawerOpen);
  };

  const match = useRouteMatch();
  const checkIfActive = (path) => {
    return match.url === path;
  };

  const allLinks = [
    <Link key={0} to={ROUTE_PREFIXES.homeRoutes.howItWorks}>
      <NavItem active={checkIfActive(ROUTE_PREFIXES.homeRoutes.howItWorks)}>
        how it works
      </NavItem>
    </Link>,
    <Link key={1} to={ROUTE_PREFIXES.homeRoutes.resume}>
      <NavItem active={checkIfActive(ROUTE_PREFIXES.homeRoutes.resume)}>
        resume
      </NavItem>
    </Link>,
    <Link key={2} to={ROUTE_PREFIXES.homeRoutes.whyBarRaiser}>
      <NavItem active={checkIfActive(ROUTE_PREFIXES.homeRoutes.whyBarRaiser)}>
        why BarRaiser
      </NavItem>
    </Link>,
    <Link key={3} to={ROUTE_PREFIXES.homeRoutes.stories}>
      <NavItem active={checkIfActive(ROUTE_PREFIXES.homeRoutes.stories)}>
        stories
      </NavItem>
    </Link>,
    <Link key={4} to={ROUTE_PREFIXES.homeRoutes.faqs}>
      <NavItem active={checkIfActive(ROUTE_PREFIXES.homeRoutes.faqs)}>
        FAQs
      </NavItem>
    </Link>,
  ];

  const buttons = [
    <Link key="button0" to={ROUTE_PREFIXES.about} className="button mr">
      <LineButton active={checkIfActive(ROUTE_PREFIXES.about)}>
        about us
      </LineButton>
    </Link>,
    <Link key="buttton1" to={ROUTE_PREFIXES.contact} className="button mr32">
      <LineButton active={checkIfActive(ROUTE_PREFIXES.contact)}>
        contact us
      </LineButton>
    </Link>,
  ];

  return (
    <StyledHeader>
      <StyledRow>
        <Col xs={23} lg={3}>
          <Link to={ROUTE_PREFIXES.home}>
            <BrandLogo />
          </Link>
        </Col>
        <Col xs={0} lg={16} className="links">
          {allLinks}
        </Col>
        <StyledHeaderButtonsSection xs={0} lg={5}>
          <Flex alignItems="center" justifyContent="flex-end">
            {buttons}
            <Logout />
          </Flex>
        </StyledHeaderButtonsSection>
        <Col xs={1} lg={0}>
          <Flex alignItems="center" justifyContent="flex-end">
            <Icon type="MenuOutlined" onClick={fnToggleDrawerOpenState} />
          </Flex>
          <Drawer
            placement="right"
            closable
            onClose={fnToggleDrawerOpenState}
            visible={isDrawerOpen}
          >
            <StyledDrawerRow>
              {allLinks.map((linkDom, index) => {
                return (
                  <Col key={`mobile-links-${index}`} xs={24} lg={0}>
                    {linkDom}
                  </Col>
                );
              })}
              <div className="separator" />
              {buttons}
              <div className="separator" />
              <SocialLinks />
            </StyledDrawerRow>
          </Drawer>
        </Col>
      </StyledRow>
    </StyledHeader>
  );
}

export const StyledBigHeader = styled.div`
  ${(props) => {
    return {
      fontSize: "xx-large",
      fontWeight: "bold",
      color: "#0390fc",
      textAlign: "center",
    };
  }}
`;
