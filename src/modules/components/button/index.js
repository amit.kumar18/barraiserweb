import styled from "styled-components";
import { Button } from "components";

export const StyledButton = styled(Button)`
  ${(props) => {
    return {
      width: "100%",
      margin: 6,
    };
  }}
`;
