import React, { useEffect, useMemo, useRef, useState } from "react";
import moment from "moment";
import styled from "styled-components";

import { Flex, Icon, Modal, Timeline } from "components";
import YouTube from "react-youtube";
import { getYoutubeIDFromURL } from "./utils";
import { VIDEO_MODAL_MODES } from "./constants";

import "./index.css";
import { Rating } from "../rating";
import { DifficultyTag } from "../difficulty-tag";
import { Collapse } from "antd";

const StyledModal = styled(Modal)`
  ${(props) => {
    return {
      top: "auto !important",
      overflow: "hidden",

      ".ant-modal-content": {
        height: "inherit",
        display: "flex",
        flexDirection: "column",
      },

      ".ant-modal-header": {
        border: "1px solid #f0f0f0",
      },

      ".ant-modal-body": {
        padding: "0px",
        flex: 1,
        display: "flex",
        flexDirection: "column",
        height: "0px",
      },

      ".ant-timeline": {
        overflowY: "scroll",
        padding: "24px",
      },
    };
  }}
`;

const StyledHighlightTextContainer = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints } = theme;

    return {
      "&.section": {
        flexWrap: "wrap",
        ".text": {
          minWidth: "250px",
        },
        ".time": {
          fontWeight: "bold",
        },
        ".question-wrapper": {
          display: "flex",
          flexDirection: "row",
          ".question": {
            flex: 1,
            fontWeight: 500,
          },
        },
        ".extra-info-container": {
          flexWrap: "wrap",
        },
      },
      ".time": {
        marginRight: 12,
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export const InterviewVideoModal = ({
  visible,
  interviewData = {},
  onCancel,
  onExpandClick,
  onMinimiseClick,
  startTime,
  mode,
}) => {
  const { youtubeLink } = interviewData;
  const youtubeId = getYoutubeIDFromURL(youtubeLink);
  let playerRef = useRef(null);
  const { Panel } = Collapse;
  const [currentQuestionTime, setCurrentQuestionTime] = useState(null);
  const [questionCollapsed, setQuestionCollapsed] = useState({});

  const [allQuestions, timeStamps] = useMemo(() => {
    let aq = [];
    let ts = [];
    let qc = {};
    if (interviewData && interviewData.questions) {
      interviewData.questions.forEach((q) => {
        if (!q.irrelevant) {
          aq.push(q);
          ts.push(q.startTime);
          qc[q.startTime] = false;
          q.followUpQuestions.forEach((x) => {
            if (!x.irrelevant) {
              aq.push(x);
              ts.push(x.startTime);
              qc[x.startTime] = false;
            }
          });
        }
      });
      if (Object.keys(questionCollapsed).length !== Object.keys(qc).length) {
        setQuestionCollapsed(qc);
      }
    }
    return [aq, ts];
  }, [interviewData]);

  const getModeSpecificModalProps = (mode = VIDEO_MODAL_MODES.modal) => {
    var propsObj = {
      [VIDEO_MODAL_MODES.thumbnail]: {
        mask: false,
        wrapClassName: "thumbnail-video-modal-mask",
        style: {
          right: 0,
          bottom: 0,
          top: "initial",
          paddingRight: 24,
        },
        bodyStyle: {
          padding: 0,
        },
        title: (
          <Flex alignItems="center">
            <Icon
              type="ExpandOutlined"
              onClick={() => {
                const currentTime = playerRef.current
                  ? playerRef.current.getCurrentTime()
                  : startTime;
                onExpandClick(interviewData, currentTime);
              }}
            />
          </Flex>
        ),
      },
      [VIDEO_MODAL_MODES.modal]: {
        style: { top: "auto", maxWidth: "1024px", height: "95vh" },
        width: "calc(100% - 48px)",
        title: (
          <Flex alignItems="center">
            <Icon
              type="CompressOutlined"
              onClick={() => {
                const currentTime = playerRef.current
                  ? playerRef.current.getCurrentTime()
                  : startTime;
                onMinimiseClick(interviewData, currentTime);
              }}
            />
          </Flex>
        ),
      },
    };
    return propsObj[mode] || {};
  };

  const handleOnReady = (event) => {
    playerRef.current = event.target;
  };

  const handleOnPlay = (event) => {
    let ct = event.target.playerInfo.currentTime;
    timeStamps.every((ts, i) => {
      if (ts <= ct && (i === timeStamps.length - 1 || timeStamps[i + 1] > ct)) {
        setCurrentQuestionTime(ts);
        let qc = JSON.parse(JSON.stringify(questionCollapsed));
        qc[ts] = true;
        setQuestionCollapsed(qc);
        return false;
      }
      return true;
    });
  };

  const seekTo = (val) => {
    playerRef.current && playerRef.current.seekTo(val);
    try {
      playerRef.current.f.focus();
    } catch (e) {}
  };

  useEffect(() => {
    if (currentQuestionTime) {
      let question = document.getElementById(currentQuestionTime.toString());
      if (question) {
        question.scrollIntoView({ behavior: "smooth" });
      }
    }
  }, [currentQuestionTime, mode]);

  function renderExtraFeedbackInformation(question) {
    return question.categoryId == 52
      ? null
      : (question.difficulty || question.rating) && (
          <div className="text flex1" key={question.id}>
            <Flex alignItems="right" className="extra-info-container">
              {question.rating && <Rating rating={question.rating} />}
              {question.difficulty && (
                <DifficultyTag difficulty={question.difficulty}>
                  {question.difficulty}
                </DifficultyTag>
              )}
            </Flex>
            <div className="feedback">{question.feedback}</div>
          </div>
        );
  }

  const renderHighlightsTimeline = (interviewData = {}) => {
    const { questions = [] } = interviewData;

    const timelineContent = questions.map((q) => (
      <Timeline.Item className="cursor-pointer">
        <StyledHighlightTextContainer id={q.startTime} className="section">
          <div className="time">
            {moment()
              .set({
                hours: 0,
                minutes: 0,
                seconds: q.startTime,
              })
              .format("H:mm:ss")}
          </div>
          <div className="text flex1">
            <Collapse
              ghost={true}
              bordered={false}
              expandIconPosition="right"
              activeKey={questionCollapsed[q.startTime] ? [q.startTime] : []}
              onChange={(event) => {
                let qc = JSON.parse(JSON.stringify(questionCollapsed));
                qc[q.startTime] = !qc[q.startTime];
                setQuestionCollapsed(qc);
              }}
            >
              <Panel
                header={
                  <div className="question-wrapper">
                    <div className="question">{q.question}</div>
                    <Icon
                      type="PlayCircleOutlined"
                      style={{ alignSelf: "center" }}
                      onClick={(event) => {
                        seekTo(q.startTime);
                        event.stopPropagation();
                      }}
                    />
                  </div>
                }
                key={q.startTime}
              >
                <p>
                  {q.feedbacks.map((f) => {
                    return renderExtraFeedbackInformation(f);
                  })}
                </p>
              </Panel>
            </Collapse>
          </div>
        </StyledHighlightTextContainer>
      </Timeline.Item>
    ));
    return timelineContent.length ? (
      <Timeline className="pt24">{timelineContent}</Timeline>
    ) : null;
  };

  const playerOptions = {
    width: "100%",
    // height: "500px",
    playerVars: {
      autoplay: 1,
      rel: 0,
      ...(startTime !== undefined && { start: startTime }),
      modestbranding: 1,
    },
  };

  return (
    <StyledModal
      {...getModeSpecificModalProps(mode)}
      visible={visible}
      centered
      footer={null}
      onCancel={() => {
        onCancel && onCancel(interviewData);
      }}
    >
      {youtubeId && (
        <YouTube
          videoId={youtubeId}
          onReady={handleOnReady}
          opts={playerOptions}
          onPlay={handleOnPlay}
        />
      )}

      {mode === VIDEO_MODAL_MODES.modal &&
        renderHighlightsTimeline({ questions: allQuestions })}
    </StyledModal>
  );
};
