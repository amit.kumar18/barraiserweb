export function getYoutubeIDFromURL(youtubeLink) {
  const youtubeId = new URLSearchParams((youtubeLink || "").split("?")[1]).get(
    "v"
  );
  return youtubeId;
}
