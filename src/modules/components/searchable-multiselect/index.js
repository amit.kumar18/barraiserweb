import React, { useRef, useState } from "react";
import styled from "styled-components";
import { LLMultiSelect, Input, Spin, Flex } from "components";
import { SuggestAsYouType } from "modules/components";

const StyledSearchInput = styled(Input.Search)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, BorderRadius } = theme;
    return {
      width: 200,
      borderRadius: BorderRadius.default,
      [`@media (max-width: ${Breakpoints.md})`]: {
        width: "100%",
      },
    };
  }}
`;

export const SearchableMultiSelect = ({
  searchPlaceholder = "",
  serachContext,
  searchResultslimit = 10,
  searchDelay = 300,
  onChange = () => { },
  labelKey,
  valueKey,
  defaultSelectedValues = [],
}) => {
  const inputEl = useRef(null);
  const [selectedVals, setSelectedVals] = useState(defaultSelectedValues);

  const updateSelectedAndNotifyParen = (newSelectedVals) => {
    setSelectedVals(newSelectedVals);
    onChange(newSelectedVals.map((o) => o[valueKey]));
  };

  const handleSelectedListCrossClick = (obj) => {
    var newSelectedVals = selectedVals.slice();
    const index = selectedVals.findIndex((o) => o[valueKey] === obj[valueKey]);
    newSelectedVals.splice(index, 1);

    updateSelectedAndNotifyParen(newSelectedVals);
  };

  const handleCheckboxChange = (cbObject, checked) => {
    var newSelectedVals = selectedVals.slice();
    if (checked) {
      newSelectedVals.push(cbObject);
    } else {
      const index = selectedVals.findIndex(
        (o) => o[valueKey] === cbObject[valueKey]
      );
      newSelectedVals.splice(index, 1);
    }

    updateSelectedAndNotifyParen(newSelectedVals);
  };

  return (
    <SuggestAsYouType
      context={serachContext}
      delay={searchDelay}
      limit={searchResultslimit}
      render={({ fetching, onSearch, data }) => {
        return (
          <LLMultiSelect.Main
            toolbar={
              <LLMultiSelect.Toolbar>
                <StyledSearchInput
                  ref={inputEl}
                  onSearch={onSearch}
                  allowClear
                  placeholder={searchPlaceholder}
                />
              </LLMultiSelect.Toolbar>
            }
            selectionDisplayList={
              <LLMultiSelect.SelectionDisplayList
                data={selectedVals}
                labelKey={labelKey}
                valueKey={valueKey}
                onCrossClick={handleSelectedListCrossClick}
              />
            }
          >
            {fetching ? (
              <Flex flex={1} alignItems="center" justifyContent="center">
                <Spin />
              </Flex>
            ) : (
              <LLMultiSelect.OptionsContainer
                data={data}
                labelKey={labelKey}
                valueKey={valueKey}
                onChange={handleCheckboxChange}
                value={selectedVals.map((o) => o[valueKey])}
              />
            )}
          </LLMultiSelect.Main>
        );
      }}
    />
  );
};
