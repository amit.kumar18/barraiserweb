import React, { PureComponent, Suspense } from "react";
import { CircularProgress } from "@material-ui/core";

export function LazyLoader(WrappedComponent) {
  class Wrapped extends PureComponent {
    render() {
      return (
        <Suspense fallback={<CircularProgress />}>
          <WrappedComponent {...this.props} />
        </Suspense>
      );
    }
  }
  return <Wrapped />;
}
