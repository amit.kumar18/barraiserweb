import styled from "styled-components";

const StyledSeperator = styled.div`
  ${props => {
    const { theme } = props;
    const { Palette, BorderRadius } = theme;
    return {
      position: "relative",
      height: 4,
      width: "100%",
      "&:after": {
        content: "''",
        height: "100%",
        width: 80,
        background: Palette.background.yellow,
        position: "absolute",
        left: "50%",
        transform: "translateX(-50%)",
        borderRadius: BorderRadius.default
      }
    };
  }}
`;

export const Seperator = StyledSeperator;
