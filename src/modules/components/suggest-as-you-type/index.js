import { useState, useCallback } from "react";
import { useApolloClient } from "@apollo/react-hooks";

import { debounce } from "src/utils";
import { FETCH_SUGGESTIONS } from "./graphql";

export const SuggestAsYouType = (props) => {
  const {
    context,
    render,
    delay,
    limit,
    minCharacters = 1,
    additionalFilters = [],
    ...restProps
  } = props;
  const [responseState, setResponseState] = useState({
    data: [],
    fetching: false,
  });

  if (!props.render || typeof props.render !== "function") {
    throw new Error(
      "SuggestAsYouType: render prop expects a function but got " +
      typeof render
    );
  }

  if (!SuggestAsYouType.Contexts[props.context]) {
    throw new Error(
      "SuggestAsYouType: invalid 'context' value. Only SuggestAsYouType.Contexts values allowed"
    );
  }

  if (props.delay && typeof props.delay !== "number") {
    throw new Error("SuggestAsYouType: 'delay' should be a number");
  }

  if (props.limit && typeof props.limit !== "number") {
    throw new Error("SuggestAsYouType: 'limit' should be a number");
  }

  if (props.minCharacters && typeof props.minCharacters !== "number") {
    throw new Error("SuggestAsYouType: 'minCharacters' should be a number");
  }

  const client = useApolloClient();
  let observable = null;

  const unsubscribeObservable = () => {
    observable && observable.unsubscribe && observable.unsubscribe();
  };

  const onSearch = useCallback(
    debounce((searchValue) => {
      const trimmedSearchValue = (searchValue || "").trim();
      if (trimmedSearchValue.length >= minCharacters) {
        // cancelling any active call
        unsubscribeObservable();

        // Updating state - fetching true
        setResponseState({
          fetching: true,
          data: [],
        });

        // Firing fresh query
        const query = client.watchQuery({
          query: FETCH_SUGGESTIONS,
          variables: {
            query: trimmedSearchValue,
            filters: [
              ...additionalFilters,
              {
                key: "context",
                value: context,
              },
            ],
            limit,
          },
        });

        // Subscribe to the query
        observable = query.subscribe(({ data }) => {
          setResponseState(
            Object.assign(
              {},
              {
                fetching: false,
                data:
                  (data.suggestAsYouType &&
                    JSON.parse(JSON.stringify(data.suggestAsYouType))) ||
                  [],
              }
            )
          );

          // unsubscribing as soon as we receive results
          unsubscribeObservable();
        });
      }
    }, delay || 0),
    []
  );

  return render({
    fetching: responseState.fetching,
    onSearch,
    data: responseState.data,
    ...restProps,
  });
};

SuggestAsYouType.Contexts = {
  skills: "skills",
  companies: "companies",
  interests: "interests"
};

// var aaa = execute(client.link, {
//   query: FETCH_SUGGESTIONS,
//   variables: {
//     context: "skill",
//     query: "tech",
//   },
// }).subscribe({
//   next: (data) =>
//     console.log(`received data: ${JSON.stringify(data, null, 2)}`),
//   error: (error) => console.log(`received error ${error}`),
//   complete: () => console.log(`complete`),
// });

// aaa.unsubscribe();

// const fetchSuggestions = client.query({
//   query: FETCH_SUGGESTIONS,
//   variables: {
//     context: "skill",
//     query: "tech",
//   },
// });

// var query = client.watchQuery({
//   query: FETCH_SUGGESTIONS,
//   variables: {
//     context: "skill",
//     query: "tech",
//   },
//   fetchPolicy: "cache-and-network",
// });

// // Subscribe to it, and do something with the data
// const observable = query.subscribe(({ data }) => {
//   // do something with `data`
//   // ...
// });

// // Then somewhere you want to cancel it...
// observable.unsubscribe();
