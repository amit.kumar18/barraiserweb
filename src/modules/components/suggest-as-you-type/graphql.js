import gql from "graphql-tag";

export const FETCH_SUGGESTIONS = gql`
  query search($filters: [SearchFilter]!, $query: String!) {
    suggestAsYouType(filters: $filters, query: $query) {
      id
      value
    }
  }
`;
