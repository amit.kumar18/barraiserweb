import React, { useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { useLazyQuery, useQuery } from "@apollo/react-hooks";
import { FETCH_INTERVIEWERS, FETCH_TARGET_JOB_ATTRIBUTES } from "./graphql";
import {
  Form,
  FormItem,
  Flex,
  Select,
  Spin,
  Button,
  Drawer,
  Dropdown,
  Radio,
} from "components";

import {
  SuggestAsYouType,
  SearchableMultiSelect,
} from "modules/components";

import { InterviewerSelectionCard } from "./interviewer-selection-card";
import { START_APPLYING_PERIODS } from "modules/constants";

const { Option } = Select;

const StyledFiltersForm = styled(Form)`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints } = theme;
    return {
      display: "flex",
      "&>div": {
        flex: 1,
        marginRight: Spacing[12],
        maxWidth: "25%",
      },
      "&>div:last-child": {
        marginRight: 0,
      },
      ".ant-form-item-label label": {
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        overflow: "hidden",
        display: "block",
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StylesMobileFilters = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { Spacing, FontWeights } = theme;
    return {
      flexDirection: "column",
      height: "100%",

      ".heading-container": {
        padding: Spacing[16],
        borderBottom: "1px solid #f2f2f2",
        fontWeight: FontWeights.bold,
      },

      ".filters-body-container": {
        flex: 1,
        display: "flex",
        overflow: "hidden",
      },

      ".filter-menu": {
        overflow: "auto",
        background: "#f2f2f2",
        maxWidth: "30%",
        ".menu-item": {
          padding: Spacing[16],
          borderBottom: "1px solid #e0e0e0",

          "&.active": {
            background: "rgba(153, 153, 153, 0.3)",
          },
        },
      },

      ".filter-body": {
        overflow: "auto",
        flex: 1,
      },
    };
  }}
`;

const getProcessedDefFiltersValsFrmTargetJobData = ({
  companies = [],
  desiredRole = {},
  skillsToFocus = [],
  timeToStartApplications = "",
} = {}) => {
  const processedSkillsToFocus = (skillsToFocus || [])
    .map((a) => a && a.id)
    .filter((a) => a);

  const processedCompanies = companies.map((a) => a && a.id).filter((a) => a);

  return {
    ...(desiredRole && desiredRole.id && { desiredRole: desiredRole.id }),
    ...(timeToStartApplications && {
      timeToStartApplications,
    }),
    ...(processedCompanies.length && { companies: processedCompanies }),
    ...(processedSkillsToFocus.length && {
      skillsToFocus: processedSkillsToFocus,
    }),
  };
};

const InterviewerSelectionFilters = (props) => {
  const isMobileScreen = useSelector((state) => state.app.isMobileScreen);
  const [filterForm] = Form.useForm();
  const [isDrawerOpen, setDrawerOpenState] = useState(false);
  const [mobileFiltersSelectedTab, setMobileFiltersSelectedTab] = useState(0);
  const fnToggleDrawerOpenState = () => {
    setDrawerOpenState(!isDrawerOpen);
  };

  const processedDefValues = getProcessedDefFiltersValsFrmTargetJobData(
    props.defaultValues
  );

  const handleFiltersValueChange = (...args) => {
    const filterVals = args[1];
    props.onFiltersChange && props.onFiltersChange(filterVals);
  };

  if (props.loading) {
    return null;
  }

  // Mobile filters
  if (isMobileScreen) {
    return (
      <div>
        <Flex alignItems="center" justifyContent="flex-end">
          <Button onClick={fnToggleDrawerOpenState}>Filters</Button>
        </Flex>
        <Drawer
          placement="right"
          closable
          bodyStyle={{ padding: 0 }}
          width="100vw"
          onClose={fnToggleDrawerOpenState}
          visible={isDrawerOpen}
        >
          <StylesMobileFilters>
            <div className="heading-container">Filters</div>
            <div className="filters-body-container">
              <div className="filter-menu">
                <div
                  className={`menu-item ${mobileFiltersSelectedTab === 0 && "active"
                    }`}
                  onClick={() => setMobileFiltersSelectedTab(0)}
                >
                  Desired Role
                </div>

                <div
                  className={`menu-item ${mobileFiltersSelectedTab === 1 && "active"
                    }`}
                  onClick={() => setMobileFiltersSelectedTab(1)}
                >
                  Application start date
                </div>

                <div
                  className={`menu-item ${mobileFiltersSelectedTab === 2 && "active"
                    }`}
                  onClick={() => setMobileFiltersSelectedTab(2)}
                >
                  Dream companies
                </div>

                <div
                  className={`menu-item ${mobileFiltersSelectedTab === 3 && "active"
                    }`}
                  onClick={() => setMobileFiltersSelectedTab(3)}
                >
                  Focus area
                </div>
              </div>
              <div className="filter-body">
                <Form
                  form={filterForm}
                  onValuesChange={handleFiltersValueChange}
                  initialValues={processedDefValues}
                >
                  <FormItem
                    name="desiredRole"
                    className={`${mobileFiltersSelectedTab !== 0 && "hidden"}`}
                  >
                    <SuggestAsYouType
                      context={SuggestAsYouType.Contexts.interests}
                      delay={300}
                      limit={10}
                      render={({ fetching, onSearch, data, onChange }) => {
                        return (
                          <Select
                            focus
                            showSearch
                            allowClear
                            key="desiredRoleSelect"
                            placeholder="Desired Role"
                            notFoundContent={
                              fetching ? <Spin size="small" /> : null
                            }
                            onSearch={onSearch}
                            onChange={onChange}
                            filterOption={false}
                            defaultValue={processedDefValues.desiredRole}
                          >
                            {data.map((d, i) => (
                              <Option key={d.id}>{d.value}</Option>
                            ))}
                          </Select>
                        );
                      }}
                    />
                  </FormItem>

                  <FormItem
                    name="timeToStartApplications"
                    className={`${mobileFiltersSelectedTab !== 1 && "hidden"}`}
                  >
                    <div className="p12">
                      <Radio.Group
                        defaultValue={
                          processedDefValues.timeToStartApplications
                        }
                      >
                        {START_APPLYING_PERIODS.map((obj, index) => (
                          <Radio
                            key={`timeToStartApplications${index}`}
                            // style={radioStyle}
                            value={obj.value}
                          >
                            {obj.label}
                          </Radio>
                        ))}
                      </Radio.Group>
                    </div>
                  </FormItem>

                  <FormItem
                    name="companies"
                    className={`${mobileFiltersSelectedTab !== 2 && "hidden"}`}
                    render={({ onChange, value }) => {
                      return (
                        <SearchableMultiSelect
                          searchPlaceholder="Dream Companies"
                          serachContext={SuggestAsYouType.Contexts.companies}
                          delay={300}
                          limit={10}
                          labelKey="value"
                          valueKey="id"
                          onChange={onChange}
                          defaultSelectedValues={(
                            props.defaultValues.companies || []
                          ).map(({ id, name }) => ({ id, value: name }))}
                          key="dreamCompaniesSelect"
                        />
                      );
                    }}
                  />

                  <FormItem
                    name="skillsToFocus"
                    className={`${mobileFiltersSelectedTab !== 3 && "hidden"}`}
                    render={({ onChange, value }) => (
                      <SearchableMultiSelect
                        searchPlaceholder="Skills to focus upon"
                        serachContext={SuggestAsYouType.Contexts.skills}
                        delay={300}
                        limit={10}
                        labelKey="value"
                        valueKey="id"
                        onChange={onChange}
                        key="skillsSelect"
                      />
                    )}
                  />
                </Form>
              </div>
            </div>
          </StylesMobileFilters>
        </Drawer>
      </div>
    );
  }

  // Desktop filters
  return (
    <StyledFiltersForm
      layout="vertical"
      form={filterForm}
      initialValues={processedDefValues}
      onValuesChange={handleFiltersValueChange}
    >
      <FormItem name="desiredRole" label="Desired Role" className="left">
        <SuggestAsYouType
          context={SuggestAsYouType.Contexts.interests}
          delay={300}
          limit={10}
          render={({ fetching, onSearch, data, onChange }) => {
            return (
              <Select
                showSearch
                allowClear
                key="desiredRoleSelect"
                placeholder="Desired Role"
                notFoundContent={fetching ? <Spin size="small" /> : null}
                onSearch={onSearch}
                onChange={onChange}
                filterOption={false}
                defaultValue={processedDefValues.desiredRole}
              >
                {data.map((d, i) => (
                  <Option key={d.id}>{d.value}</Option>
                ))}
              </Select>
            );
          }}
        />
      </FormItem>

      <FormItem
        name="timeToStartApplications"
        label="When do you want to start applying?"
        className="right"
      >
        <Select
          placeholder="When do you want to start applying?"
          allowClear
          defaultValue={processedDefValues.timeToStartApplications}
        >
          {START_APPLYING_PERIODS.map((obj, index) => (
            <Option key={`timeToStartApplications${index}`} value={obj.value}>
              {obj.label}
            </Option>
          ))}
        </Select>
      </FormItem>

      <FormItem
        name="companies"
        label="Dream Companies"
        className="left"
        render={({ onChange, value }) => {
          return (
            <Dropdown
              overlay={
                <SearchableMultiSelect
                  searchPlaceholder="Dream Companies"
                  serachContext={SuggestAsYouType.Contexts.companies}
                  delay={300}
                  limit={10}
                  labelKey="value"
                  valueKey="id"
                  onChange={onChange}
                  defaultSelectedValues={(
                    props.defaultValues.companies || []
                  ).map(({ id, name }) => ({ id, value: name }))}
                  key="dreamCompaniesSelect"
                />
              }
              trigger={["click"]}
            >
              <Select
                placeholder={"Dream companies"}
                notFoundContent={null}
                {...(value &&
                  value.length && { value: `${value.length} selected` })}
              />
            </Dropdown>
          );
        }}
      />

      <FormItem
        name="skillsToFocus"
        label="Skills to focus upon"
        className="left"
        render={({ onChange, value }) => (
          <Dropdown
            overlay={
              <SearchableMultiSelect
                searchPlaceholder="Skills to focus upon"
                serachContext={SuggestAsYouType.Contexts.skills}
                delay={300}
                limit={10}
                labelKey="value"
                valueKey="id"
                onChange={onChange}
                key="skillsSelect"
              />
            }
            trigger={["click"]}
          >
            <Select
              placeholder={"Skills to focus upon"}
              notFoundContent={null}
              {...(value &&
                value.length && { value: `${value.length} selected` })}
            />
          </Dropdown>
        )}
      />
    </StyledFiltersForm>
  );
};

export function InterviewerSelection(props) {
  // Fetching data
  const [
    fetchInterviewersAPI,
    {
      loading: fetchingInterviewers,
      data: interviewersData = {
        getInterviewers: [],
      },
    },
  ] = useLazyQuery(FETCH_INTERVIEWERS, {
    onError: (args) => {
      alert("Some error occured", args);
    },
  });

  const {
    getInterviewers: interviewrsForSelectedFilters = [],
  } = interviewersData;

  const {
    loading: fetchingDefaultFilterVals,
    data: targetJobAttributesData = {
      getTargetJobAttributes: {},
    },
  } = useQuery(FETCH_TARGET_JOB_ATTRIBUTES, {
    onCompleted: ({ getTargetJobAttributes = {} }) => {
      handleFiltersValueChange(
        getProcessedDefFiltersValsFrmTargetJobData(getTargetJobAttributes)
      );
    },
    onError: (args) => {
      alert("Some error occured", args);
    },
  });

  const {
    getTargetJobAttributes: targetJobAttributes = {},
  } = targetJobAttributesData;

  const handleFiltersValueChange = (filterVals) => {
    const {
      timeToStartApplications,
      companies,
      desiredRole,
      skillsToFocus,
    } = filterVals;

    fetchInterviewersAPI({
      variables: {
        input: {
          ...(timeToStartApplications && {
            timeToStartApplications,
          }),
          dreamJobAttributes: {
            ...(companies && { companies }),
            ...(desiredRole && { desiredRole }),
            ...(skillsToFocus && { skillsToFocus }),
          },
          ...(props.interviewRound && {
            interviewRound: props.interviewRound,
          }),
        },
      },
    });
  };

  const handleInterviewerSelection = (...args) => {
    const { onSelection = () => { } } = props;
    onSelection(...args);
  };

  let content = null;
  if (fetchingInterviewers) {
    content = (
      <Flex justifyContent="center" alignItems="center">
        <Spin />
      </Flex>
    );
  } else {
    if (!interviewrsForSelectedFilters.length) {
      content =
        "No data found for the selected filters. Please update the filters and try again.";
    } else {
      content = interviewrsForSelectedFilters.map((data) => {
        return (
          <InterviewerSelectionCard
            data={data}
            onSelectionComplete={handleInterviewerSelection}
            interviewRound={props.interviewRound}
          />
        );
      });
    }
  }

  return (
    <>
      <InterviewerSelectionFilters
        onFiltersChange={handleFiltersValueChange}
        defaultValues={targetJobAttributes}
        loading={fetchingDefaultFilterVals}
      />
      <div className="mt8">{content}</div>
    </>
  );
}
