import React, { useState, useEffect } from "react";
import styled from "styled-components";
import moment from "moment";
import { useLazyQuery } from "@apollo/react-hooks";

import InterviewerCompaniesDemo from "assets/images/interviewers_companies_demo.png";

import { FETCH_INTERVIEWER_AVAILABILITY_SLOTS } from "./graphql";
import {
  Form,
  FormItem,
  BrandButton,
  DatePicker,
  Icon,
  Button,
} from "components";

const StyledDatePickerOverlay = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints } = theme;
    return {
      display: "flex",
      ".loading-slots": {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        background: "rgba(255, 255, 255, 0.8)",
        zIndex: 1000,
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StyledRecommendedBadge = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints, Palette, FontSizes } = theme;
    return {
      display: "inline-flex",
      alignItems: "center",
      padding: Spacing[12],
      fontSize: FontSizes.smallText,
      borderTopRightRadius: "4px",
      borderBottomRightRadius: "4px",
      background: Palette.background.yellow,
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StyledInterviewerCard = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints, Palette, FontSizes } = theme;
    return {
      padding: Spacing[16],
      background: Palette.background.white,
      maxWidth: 900,
      margin: "0 auto",
      marginBottom: Spacing[24],

      ".recommended-container": {
        marginLeft: "-16px",
        marginBottom: Spacing[16],
      },

      ".content-row": {
        display: "flex",
        ".companies-logo-container": {
          maxWidth: 120,
        },
        "&>div": {
          marginRight: Spacing[16],
        },
        "&>div:last-child": {
          marginRight: 0,
        },
      },
      ".details-container": {
        fontSize: FontSizes.mobileDefault,
        "&>*": {
          marginBottom: Spacing[8],
        },
        ".name": {
          fontSize: FontSizes.mediumText,
          display: "inline-block",
        },
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        ".content-row": {
          flexDirection: "column",
          alignItems: "center",
          textAlign: "center",
        },
      },
    };
  }}
`;

export const InterviewerSelectionCard = ({
  data: {
    id,
    achievements = [],
    cost,
    currentCompany = {},
    designation = "",
    initials = "",
    lastCompanies = [],
    recommended = false,
    almaMater = "",
    availability = [],
  },
  onSelectionComplete = () => {},
  interviewRound,
}) => {
  const [form] = Form.useForm();
  const [selectedDate, setSelectedDate] = useState(
    availability && availability[0]
      ? moment.unix(availability[0].startDate)
      : moment()
  );

  const [
    availableSlotsForSelectedDate,
    setAvailableSlotsForSelectedDate,
  ] = useState([]);

  const [
    fetchAvailabilitySlotsAPI,
    { loading: fetchingSlots, data: slotsData = {} },
  ] = useLazyQuery(FETCH_INTERVIEWER_AVAILABILITY_SLOTS, {
    onCompleted: ({ getAvailability: slts = [] }) => {
      setAvailableSlotsForSelectedDate(getSlotsForSelectedDate(selectedDate));
    },
    onError: (args) => {
      alert("Some error occured", args);
    },
  });
  const { getAvailability: slotsForTimeFrame = [] } = slotsData;

  const fetchSlotsForSelectedDate = (selDate) => {
    fetchAvailabilitySlotsAPI({
      variables: {
        input: {
          startDate: moment.utc(selDate).startOf("month").unix(),
          endDate: moment.utc(selDate).endOf("month").unix(),
          userId: id,
        },
      },
    });
  };

  const handleDatePickerChange = (selDate) => {
    setSelectedDate(selDate);
    setAvailableSlotsForSelectedDate(getSlotsForSelectedDate(selDate));
    form.resetFields(["slotSelect"]);
  };

  const handlePanelChange = (dt) => {
    fetchSlotsForSelectedDate(dt);
  };

  // initial load data
  useEffect(() => {
    fetchSlotsForSelectedDate(selectedDate);
  }, []);

  const getSlotsForSelectedDate = (sDate) => {
    return sDate && (slotsForTimeFrame || []).length
      ? slotsForTimeFrame.filter((dt) => {
          const comp = moment.unix(dt.startDate).format("DD-MM-YYYY");
          return sDate.format("DD-MM-YYYY") === comp;
        })
      : [];
  };

  const handleSelectInterviewerClick = async () => {
    try {
      const values = await form.validateFields();
      const payload = {
        startDate: values.slotSelect.startDate,
        endDate: values.slotSelect.endDate,
        interviewerId: values.slotSelect.userId,
        interviewRound: interviewRound,
      };
      onSelectionComplete(payload);
    } catch (errorInfo) {
      console.log("Failed:", errorInfo);
    }
  };

  const getRecommendedDom = () => {
    return (
      <StyledRecommendedBadge>
        <div>Recommended</div>
        <Icon className="ml4" type="StarFilled" />
        <Icon className="ml4" type="StarFilled" />
      </StyledRecommendedBadge>
    );
  };

  return (
    <StyledInterviewerCard>
      {recommended && getRecommendedDom() && (
        <div className="recommended-container">{getRecommendedDom()}</div>
      )}
      <div className="content-row">
        <div className="companies-logo-container">
          <img
            src={InterviewerCompaniesDemo}
            style={{
              maxWidth: "100%",
              marginTop: "-10px",
            }}
          />
        </div>
        <div className="details-container flex1">
          <span className="name">
            {initials}
            {", "}
            <span className="text-bold">{designation}</span>
          </span>
          <div>
            Last companies: {lastCompanies.map((comp) => comp.name).join(", ")}
          </div>
          <div>Alma Mater: {almaMater}</div>
          <div>Achievements: {achievements.map((a) => a).join(", ")}</div>
          <div className="text-bold">
            Interview cost: {cost.currency} {cost.value}
          </div>
        </div>
        <div className="flex1">
          <Form form={form} name="dynamic_rule" layout="vertical">
            <FormItem name="datePicker" label="Select slot">
              <>
                <DatePicker
                  key="jobDetilsDatePicker"
                  className="w100"
                  value={selectedDate}
                  onChange={handleDatePickerChange}
                  format="DD-MMM-YYYY"
                  disabledDate={(current) => {
                    const hasSlots = (slotsForTimeFrame || []).reduce(
                      (agg, curr) => {
                        if (
                          moment.unix(curr.startDate).format("DD-MM-YYYY") ===
                          current.format("DD-MM-YYYY")
                        ) {
                          agg = true;
                        }
                        return agg;
                      },
                      false
                    );
                    return (
                      moment.utc(current).startOf("day") <
                        moment.utc().startOf("day") || !hasSlots
                    );
                  }}
                  renderExtraFooter={() =>
                    fetchingSlots ? (
                      <div className="loading-slots">
                        <Icon type="LoadingOutlined" variant="large" />
                      </div>
                    ) : null
                  }
                  getPopupContainer={() =>
                    document.getElementById("jobDetailsDatePickerPopup")
                  }
                  showToday={false}
                  onPanelChange={handlePanelChange}
                />
                <StyledDatePickerOverlay
                  key="jobDetilsDatePickerOverlayCont"
                  id="jobDetailsDatePickerPopup"
                />
              </>
            </FormItem>
            <FormItem
              name="slotSelect"
              label="Time"
              className="right"
              rules={[
                {
                  required: true,
                  validator: (rule, value) => {
                    if (value) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Please select a suitable slot");
                  },
                },
              ]}
              render={(formItemProps) => {
                return (
                  <div style={{ marginBottom: "-16px" }}>
                    {availableSlotsForSelectedDate &&
                    availableSlotsForSelectedDate.length
                      ? availableSlotsForSelectedDate
                          .sort((a, b) => a.startDate - b.startDate)
                          .map((a) => {
                            return (
                              <Button
                                className="mb16 mr16"
                                onClick={() => {
                                  formItemProps.onChange(a);
                                }}
                                type={
                                  formItemProps.value &&
                                  formItemProps.value.startDate === a.startDate
                                    ? "primary"
                                    : "default"
                                }
                              >
                                {moment.unix(a.startDate).format("hh:mm A")}
                              </Button>
                            );
                          })
                      : "No slots available for the selected date"}
                  </div>
                );
              }}
            />
          </Form>
          <div>
            <BrandButton
              variant="secondary"
              onClick={handleSelectInterviewerClick}
              size="md"
            >
              Select Interviewer
            </BrandButton>
          </div>
        </div>
      </div>
    </StyledInterviewerCard>
  );
};
