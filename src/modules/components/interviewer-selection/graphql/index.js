import gql from "graphql-tag";

export const FETCH_INTERVIEWERS = gql`
  query fetchInterviewers($input: GetInterviewersInput!) {
    getInterviewers(input: $input) {
      id
      achievements
      almaMater
      cost {
        value
        currency
        symbol
      }
      designation
      initials
      recommended
      availability {
        endDate
        startDate
        userId
      }
      currentCompany {
        id
        logo
        name
        url
      }
      lastCompanies {
        id
        logo
        name
        url
      }
    }
  }
`;

export const FETCH_TARGET_JOB_ATTRIBUTES = gql`
  query getTargetJobAttributes {
    getTargetJobAttributes {
      companies {
        name
        logo
        id
        url
      }
      desiredRole {
        name
        id
      }
      skillsToFocus {
        id
        name
      }
      timeToStartApplications
    }
  }
`;

export const FETCH_INTERVIEWER_AVAILABILITY_SLOTS = gql`
  query fetchInterviewerAvailability($input: GetAvailabilityInput!) {
    getAvailability(input: $input) {
      endDate
      startDate
      userId
    }
  }
`;
