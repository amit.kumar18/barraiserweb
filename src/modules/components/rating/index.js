import styled from "styled-components";
import React from "react";
import { RATING_COLOR } from "modules/constants";
import { Flex, Icon } from "components";

const StyledRating = styled(Flex)`
  ${(props) => {
    const { theme, color } = props;
    const { Spacing, FontSizes, FontWeights, Breakpoints } = theme;
    return {
      alignItems: "center",
      paddingRight: Spacing[24],
      ".anticon": {
        fontSize: FontSizes.largeText,
        paddingRight: "4px",
        color: color,
      },
      ".rating-text": {
        fontWeight: FontWeights.bold,
        fontSize: FontSizes.mobileDefault,
        paddingLeft: "6px",
      },
      [`@media (max-width: ${Breakpoints.sm})`]: {
        paddingRight: Spacing[12],
        ".anticon": {
          fontSize: FontSizes.default,
          paddingRight: "0px",
        },
      },
    };
  }}
`;

export const Rating = (props) => {
  const { rating } = props;
  const color = RATING_COLOR[rating];
  let filledStars = [];
  let outlinedStars = [];
  for (let i = 0; i < rating; ++i) {
    filledStars.push(<Icon type="StarFilled" />);
  }
  for (let i = 0; i < 10 - rating; ++i) {
    outlinedStars.push(<Icon type="StarOutlined" />);
  }
  return (
    <StyledRating color={color}>
      {filledStars}
      {outlinedStars}
      <span className="rating-text">{rating}/10</span>
    </StyledRating>
  );
};
