import React from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "components";
import styled from "styled-components";

import { ROUTE_PREFIXES } from "constantVars";
import { FooterCopyRight } from "./copyright";

import { SocialLinks } from "modules/components";

const StyledFooter = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Palette, Spacing, FontSizes } = theme;
    return {
      background: Palette.background.offWhite,
      padding: `${Spacing[40]} ${Spacing[32]}`,
      fontSize: FontSizes.smallText,
      ".link": {
        marginBottom: Spacing[16],
        color: Palette.text.default,
        display: "inline-flex",
      },
      ".social-links-sect": {
        display: "flex",
        justifyContent: "flex-end",
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        padding: `${Spacing[32]} ${Spacing[12]}`,
        textAlign: "center",
        ".social-links-sect": {
          marginTop: Spacing[40],
          display: "flex",
          justifyContent: "center",
        },
      },
    };
  }}
`;

export function PageFooter() {
  return (
    <div>
      <StyledFooter>
        <Row type="flex" justify="space-between" align="middle">
          <Col xs={24} md={12} lg={8} style={{ marginBottom: "-16px" }}>
            <Row style={{ flex: 1 }}>
              <Col xs={24} md={12}>
                <Link
                  className="link"
                  to={ROUTE_PREFIXES.homeRoutes.howItWorks}
                >
                  How it works
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link className="link" to={ROUTE_PREFIXES.homeRoutes.resume}>
                  Sample resume
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link
                  className="link"
                  to={ROUTE_PREFIXES.homeRoutes.whyBarRaiser}
                >
                  why BarRaiser
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link className="link" to={ROUTE_PREFIXES.homeRoutes.stories}>
                  Stories from top interviwers
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link className="link" to={ROUTE_PREFIXES.homeRoutes.faqs}>
                  FAQs
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link className="link" to={ROUTE_PREFIXES.about}>
                  About us
                </Link>
              </Col>
              <Col xs={24} md={12}>
                <Link className="link" to={ROUTE_PREFIXES.privacyPolicy}>
                  Privacy policy
                </Link>
              </Col>
            </Row>
          </Col>
          <Col className="social-links-sect" xs={24} md={8}>
            <SocialLinks />
          </Col>
        </Row>
      </StyledFooter>
      <FooterCopyRight />
    </div>
  );
}
