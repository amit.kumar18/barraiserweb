import React from "react";
import styled from "styled-components";
import { Icon } from "components";

const StyledCopyRightSect = styled.div`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints, Palette, Spacing, FontSizes } = theme;
    return {
      display: "flex",
      alignItems: "center",
      background: Palette.background.darkBlue,
      padding: `${Spacing[12]} ${Spacing[32]}`,
      color: Palette.text.white,
      ".text": {
        display: "inline-flex",
        paddingLeft: Spacing[12],
        fontSize: FontSizes.smallText,
      },
      [`@media (max-width: ${Breakpoints.md})`]: {
        padding: Spacing[12],
        ".text": {
          fontSize: FontSizes.smallText,
        },
      },
    };
  }}
`;

export const FooterCopyRight = () => (
  <StyledCopyRightSect>
    <Icon type="CopyrightCircleOutlined" />
    <div className="text">BarRaiser Private Limited</div>
  </StyledCopyRightSect>
);
