import styled from "styled-components";

export const Page = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const PageContent = styled.div`
  ${(props) => {
    const { bgLight } = props;
    return {
      flex: 1,
      overflow: "auto",
      display: "flex",
      flexDirection: "column",
      ...(bgLight && {
        background: "#f2f2f2",
      }),
    };
  }}
`;

export const PageContentBody = styled.div`
  flex: 1;
`;
