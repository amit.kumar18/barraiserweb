import React from "react";
import { Flex, Spin } from "components";
import {
  PageHeader,
  PageFooter,
  Page,
  PageContent,
  PageContentBody,
} from "modules/components";

export const RouteLoader = () => (
  <Page>
    <PageHeader />
    <PageContent>
      <PageContentBody>
        <Flex justifyContent="center" alignItems="center" className="h100">
          <Spin />
        </Flex>
      </PageContentBody>
      <PageFooter />
    </PageContent>
  </Page>
);
