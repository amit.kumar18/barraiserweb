import React, { useCallback, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useMutation, useLazyQuery } from "@apollo/react-hooks";
import styled from "styled-components";
import { Modal } from "components";

import { LoginOptions, OtpScreen } from "modules/components";
import { phoneOrEmailLoginCompleteAction } from "src/redux/currentUser";
import { SEND_OTP, VERIFY_OTP } from "./graphql";
import { notification } from "antd";
import { Dialog } from "@material-ui/core";
import { REGISTERED_SIGN_IN_MSG } from "./constant";

export const StyledModal = styled(Modal)`
  ${(props) => {
    const { theme } = props;
    const { Breakpoints } = theme;
    return {
      ".ant-modal-content": {},
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export const LoginModal = function () {
  // Redux state
  const dispatch = useDispatch();
  const fnMarkPhoneOrEmailLoginComplete = useCallback(
    (payload) => dispatch(phoneOrEmailLoginCompleteAction(payload)),
    [dispatch]
  );

  // history
  const history = useHistory();

  // Local state
  const [modal2Visible, setModal2Visible] = useState(true);
  const [showOTPScreen, setOTPScreenVisible] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [email, setEmail] = useState(null);

  const [sendOtpToUser, { loading }] = useMutation(SEND_OTP, {
    onCompleted: ({ sendOtp }) => {
      if (sendOtp) {
        setOTPScreenVisible(true);
      }
    },
    onError: (args) => {
      alert("Some error occured", args);
    },
  });

  const [verifyOtp, { loading: optLoading }] = useLazyQuery(VERIFY_OTP, {
    onCompleted: ({ verifyOtp: { authenticated, verified } }) => {
      let message = "";

      fnMarkPhoneOrEmailLoginComplete({
        authenticated,
        phoneNumber,
        verified,
        email,
      });

      if (authenticated && verified) {
        message = REGISTERED_SIGN_IN_MSG;
        if (history.length > 2) history.goBack();
        else history.push("/");
      } else if (verified) {
        message = "New User.. Signing Up..";
        history.replace("/signup");
      } else {
        message = "Wrong OTP entered";
      }

      notification.open({
        message,
      });
    },
    onError: (args) => {
      alert("Some error occured", args);
    },
  });

  const isModalClosable = !(history.location.state || {}).blocking;
  const onCancel = () => {
    if (isModalClosable) {
      setModal2Visible(false);
      setTimeout(() => {
        history.replace(history.location.pathname.split("/login")[0]);
      }, 100);
    }
  };

  const loginOptionsSubmitHandler = (phone, email) => {
    if (loading) {
      return;
    }
    if (phone) setPhoneNumber(phone);
    if (email) setEmail(email);
    if (phone || email) {
      sendOtpToUser({
        variables: { phone, email },
      });
    }
  };

  const otpSubmitHandler = (otp) => {
    if (optLoading) {
      return;
    }
    verifyOtp({
      variables: {
        phone: phoneNumber,
        otp,
        email,
      },
    });
  };

  return (
    <Dialog
      aria-labelledby="modal-title"
      modal={true}
      open={modal2Visible}
      onClose={onCancel}
    >
      {!showOTPScreen ? (
        <LoginOptions onSubmit={loginOptionsSubmitHandler} />
      ) : (
        <OtpScreen
          onSubmit={otpSubmitHandler}
          phone={phoneNumber}
          email={email}
        />
      )}
    </Dialog>
  );
};
