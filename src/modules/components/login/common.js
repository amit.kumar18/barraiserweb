import styled from "styled-components";
import { Flex } from "components";

export const StyledLoginModalHeader = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { FontSizes, Palette, Breakpoints, Gradients, FontWeights } = theme;
    return {
      alignItems: "center",
      justifyContent: "center",
      height: 112,
      backgroundImage: Gradients.homeBlue,
      color: Palette.text.white,
      fontSize: FontSizes.h3,
      fontWeight: FontWeights.bold,
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export const StyledLoginModalBody = styled(Flex)`
  ${(props) => {
    const { theme } = props;
    const { Spacing, Breakpoints } = theme;
    return {
      flexDirection: "column",
      alignItems: "center",
      padding: `${Spacing[24]}}`,
      maxWidth: 450,
      margin: "0 auto",
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;
