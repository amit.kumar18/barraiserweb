import React, { useState } from "react";
import { TelephoneImput, Input } from "components";

import Flags from "assets/images/flags.png";
import styled from "styled-components";
import {
  Typography,
  Box,
  Button,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { COUNTRIES, DEFAULT_COUNTRY } from "./constant";

const StyledTelephoneInput = styled(TelephoneImput)`
  ${(props) => {
    const { theme, disabled } = props;
    const { Breakpoints, FontSizes } = theme;
    return {
      width: "100%",
      "input[type='tel']": {
        backgroundColor: `${disabled ? "#f5f5f5" : "inherit"}`,
        color: `${disabled ? "rgba(0, 0, 0, 0.25)" : "inherit"}`,
        height: 56,
        paddingLeft: 72,
        boxShadow: "none",
        outline: "none",
        fontSize: FontSizes.default,
      },
      "input[type='tel']:focus": {
        borderColor: "#cacaca",
      },
      ".flag-dropdown": {
        border: "none",
      },
      ".selected-flag": {
        height: "calc(100% - 2px)",
        margin: "1px 0 1px 1px",
        padding: "0 16px",
        width: 56,
        outline: "none",
        border: "none",
        borderRight: "1px solid #cacaca",
        opacity: disabled ? 0.5 : 1,
      },
      ".country-list": {
        top: 56,
      },
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StyledEmailInput = styled(Input)`
  ${(props) => {
    const { theme } = props;
    const { Spacing, FontSizes } = theme;
    return {
      marginBottom: Spacing[24],
      marginTop: Spacing[24],
      padding: `${Spacing[12]} ${Spacing[24]}`,
      fontSize: FontSizes.default,
      height: 56,
      boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.08)",
      borderRadius: 6,
    };
  }}
`;

const StyledTelephoneInputContainer = styled.div`
  ${(props) => {
    const {
      theme: { Breakpoints, Spacing },
    } = props;
    return {
      paddingBottom: Spacing[24],
      width: "100%",
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export const LoginOptions = function (props) {
  const { onSubmit } = props;
  const [phone, setPhone] = useState("");
  const [phoneDisabled, setPhoneDisabled] = useState(false);
  const [email, setEmail] = useState("");
  const [emailDisabled, setEmailDisabled] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const submitHandler = () => {
    return onSubmit(phone || null, email || null);
  };

  /**
   * This function handles the onChange event of phone number input field.
   * If the phone number is valid then email field is disbaled and continue
   * button is enabled.
   * @param {[String]} value Phone Number
   * @param {[Object]} selectedCountry
   */
  const phoneNumberHandler = (value, selectedCountry) => {
    const validNumberLength =
      value.length - selectedCountry.dialCode.length - 1;
    if (validNumberLength === 10) {
      setPhone(value);
      setEmailDisabled(true);
    } else {
      setPhone("");
      setEmailDisabled(false);
    }
  };
  return (
    <Box p={isMobile ? "48px 20px 66px" : "48px 58px 66px 64px"}>
      <Typography variant="h3" align="center" color="textSecondary">
        Login/ Signup
      </Typography>
      <Box p={isMobile ? "17px 0 40px" : "17px 19px 77px 25px"}>
        <Typography variant="subtitle1" align="center" color="textPrimary">
          To get started, kindly enter your phone number or email and press
          continue
        </Typography>
      </Box>
      <StyledTelephoneInputContainer>
        <StyledTelephoneInput
          disabled={phoneDisabled}
          inputProps={{
            autoFocus: true,
          }}
          defaultCountry={DEFAULT_COUNTRY}
          flagsImagePath={Flags}
          autoFormat={false}
          className={`${phoneDisabled ? "disabled-phone-input" : null}`}
          onlyCountries={COUNTRIES}
          onChange={phoneNumberHandler}
          onEnterKeyPress={(e) => {
            e.preventDefault();
            submitHandler();
          }}
        />
      </StyledTelephoneInputContainer>

      <Typography variant="body1" align="center" color="textPrimary">
        OR
      </Typography>

      <StyledEmailInput
        disabled={emailDisabled}
        value={email}
        placeholder="Email"
        onChange={(e) => {
          if (e.target.value === "") setPhoneDisabled(false);
          else setPhoneDisabled(true);
          setEmail(e.target.value);
        }}
        onKeyUp={(e) => {
          if (e.key === "Enter") {
            e.preventDefault();
            submitHandler();
          }
        }}
      />

      <Button
        variant="contained"
        size="large"
        color="primary"
        align="center"
        disabled={phone === "" && email === ""}
        onClick={submitHandler}
        fullWidth
      >
        Continue
      </Button>
    </Box>
  );
};
