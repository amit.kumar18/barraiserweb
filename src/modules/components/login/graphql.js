import gql from "graphql-tag";

export const SEND_OTP = gql`
  mutation SendOtpToUser($phone: String, $email: String) {
    sendOtp(phone: $phone, email: $email)
  }
`;

export const VERIFY_OTP = gql`
  query VerifyOtp($phone: String, $email: String, $otp: String!) {
    verifyOtp(phone: $phone, email: $email, otp: $otp) {
      authenticated
      verified
    }
  }
`;

export const SIGN_OUT = gql`
  mutation SignOut {
    signOut {
      success
    }
  }
`;
