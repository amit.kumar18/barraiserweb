import React from "react";
import { Button } from "components";
import { useDispatch, useSelector } from "react-redux";
import { safeGet } from "src/utils";
import { SIGN_OUT } from "./graphql";
import { useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { ROUTE_PREFIXES } from "constantVars";
import { clearUserDetailsAction } from "src/redux/currentUser";
import { Button as MaterialButton, makeStyles } from "@material-ui/core";
import { LOGOUT_FAIL_MSG } from "./constant";

const useStyles = makeStyles((theme) => ({
  button: {
    [theme.breakpoints.down("sm")]: {
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      width: "90%",
    },
  },
}));

export function Logout({ newButton }) {
  const classes = useStyles();
  const authenticated = useSelector((state) =>
    safeGet(state, ["currentUser", "authenticated"])
  );

  const history = useHistory();
  const dispatch = useDispatch();

  const [signOut, { loading }] = useMutation(SIGN_OUT, {
    onCompleted: (apiRespData) => {
      const success = safeGet(apiRespData, ["signOut", "success"]);
      if (success) {
        history.push("/login");
        dispatch(clearUserDetailsAction());
      } else {
        alert(LOGOUT_FAIL_MSG);
      }
    },
    onError: () => {
      alert(LOGOUT_FAIL_MSG);
    },
  });

  return authenticated ? (
    newButton ? (
      <MaterialButton
        variant="outlined"
        color="primary"
        size="small"
        className={classes.button}
        onClick={() => {
          signOut();
        }}
      >
        Logout
      </MaterialButton>
    ) : (
      <Button
        shape="round"
        loading={loading}
        type="primary"
        onClick={() => {
          signOut();
        }}
      >
        logout
      </Button>
    )
  ) : null;
}
