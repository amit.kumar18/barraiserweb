export const REGISTERED_SIGN_IN_MSG = "Registered User.. Signing In..";
export const LOGOUT_FAIL_MSG = "Could not logout. Please try again later.";
export const DEFAULT_COUNTRY = "in";
export const COUNTRIES = [
  {
    name: "India",
    iso2: "in",
    dialCode: "91",
    priority: 1,
    format: "+..-.....-.....",
  },
  {
    name: "USA",
    iso2: "us",
    dialCode: "1",
    priority: 2,
    format: "+. (...) ...-....",
  },
];
