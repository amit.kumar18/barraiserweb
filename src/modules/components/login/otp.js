import React, { useState } from "react";
import styled from "styled-components";
import { OtpInput } from "components";
import { StyledLoginModalBody } from "./common";
import { Typography, Box, Button } from "@material-ui/core";

const StyledLoginText = styled.div`
  ${(props) => {
    const {
      theme: { Breakpoints, Spacing },
    } = props;
    return {
      width: "100%",
      textAlign: "center",
      paddingBottom: Spacing[24],
      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

const StyledTelephoneInputContainer = styled.div`
  ${(props) => {
    const {
      theme: { Breakpoints, FontSizes, Spacing },
    } = props;
    return {
      paddingBottom: Spacing[24],
      width: "100%",
      ".container": {
        justifyContent: "center",
        marginRight: `-${Spacing[16]}`,
      },
      ".input": {
        width: "56px !important",
        height: 56,
        fontSize: FontSizes.default,
        marginRight: Spacing[16],
      },

      [`@media (max-width: ${Breakpoints.md})`]: {},
    };
  }}
`;

export const OtpScreen = function (props) {
  const [opt, setOtp] = useState("");
  const otpSubmitHandler = () => {
    if (opt && opt.length && opt.length === 4) {
      props.onSubmit(opt);
    }
  };
  return (
    <Box p="48px 58px 66px 64px">
      <Typography variant="h3" align="center" color="textSecondary">
        Enter OTP
      </Typography>
      <StyledLoginModalBody>
        <StyledLoginText>
          <div>
            {"Enter the OTP received on your " +
              (props.phone !== null ? "phone number" : "email id")}
          </div>
          <div className="text-bold">
            {props.phone !== null ? props.phone : props.email}
          </div>
        </StyledLoginText>

        <StyledTelephoneInputContainer>
          <div
            onKeyUp={(e) => {
              if (e.key === "Enter") {
                otpSubmitHandler();
              }
            }}
          >
            <OtpInput
              value={opt}
              onChange={setOtp}
              inputStyle="input"
              shouldAutoFocus
              containerStyle="container"
            />
          </div>
        </StyledTelephoneInputContainer>

        <Button
          variant="contained"
          size="large"
          color="primary"
          onClick={otpSubmitHandler}
          fullWidth
        >
          Continue
        </Button>
      </StyledLoginModalBody>
    </Box>
  );
};
