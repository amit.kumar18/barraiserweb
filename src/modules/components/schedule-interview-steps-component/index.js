import React from "react";
import { Steps } from "components";

export const ScheduleInterviewStepsComponent = ({
  activeStep = 0,
  ...restProps
}) => {
  const stepsConfig = [
    {
      key: 0,
      title: "Job details",
    },
    {
      key: 1,
      title: "Peer interview",
    },
    {
      key: 2,
      title: "Expert interview",
    },
    {
      key: 3,
      title: "Payment",
    },
  ];
  return (
    <Steps
      activeStep={activeStep}
      direction="horizontal"
      labelPlacement="vertical"
      showOnlyActiveStepTitle
      stepsConfig={stepsConfig}
      {...restProps}
    ></Steps>
  );
};
