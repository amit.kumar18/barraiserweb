import gql from "graphql-tag";

export const FETCH_KPI_DATA = gql`
  query getDisplayKPINumbers {
    getDisplayKPINumbers {
      numberOfInterviews
      numberOfExperts
      numberOfPartners
    }
  }
`;
