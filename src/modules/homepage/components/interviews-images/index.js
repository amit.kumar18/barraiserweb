import React from "react";
import {
  Box,
  makeStyles,
  Typography,
  Grid,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import { BRContainer } from "src/modules/components";

const useStyles = (bgImage) =>
  makeStyles((theme) => {
    return {
      root: {
        backgroundImage: `url(${bgImage})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "contain",
        "@media: (max-width: 518)": {
          backgroundSize: "contain",
        },
      },
      img: {
        position: "relative",
        width: "250px",
        height: "150px",

        borderRadius: "20px",
        backgroundColor: theme.palette.primary.main,
      },
      imgGrid: {
        width: "auto",
      },
    };
  });

export const InterviewsImages = () => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const bgImage = isMobile
    ? getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["interviewimages_bg_mobile"])
    : getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
      "interview_images_background",
    ]);
  const classes = useStyles(bgImage)();
  const images = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["collage"]);

  return (
    // <BRContainer
    //   maxWidth={false}
    //   className={classes.root}
    //   backgroundImage={bgImage}
    // >
    <>
      <Box display="flex" justifyContent="center">
        <Typography variant="h2">
          <Box
            textAlign="center"
            color="white"
            pt={isMobile ? 25 : 30}
            mb={isMobile ? 5 : -60}
            width={isMobile ? "90%" : "75%"}
            margin="auto"
          >
            {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
              "interview_images_header",
            ])}
          </Box>
        </Typography>
      </Box>
      <Grid
        container
        direction="row"
        className={classes.imgGrid}
        justify="center"
        wrap="nowrap"
      >
        {!isMobile && (
          <Box display="flex" flexDirection="column-reverse">
            {images &&
              images
                .slice(0, 4)
                .map((obj) => <img src={obj.image} className={classes.img} />)}
          </Box>
        )}
        {!isMobile && (
          <Box display="flex" flexDirection="column-reverse">
            {images &&
              images
                .slice(4, 6)
                .map((obj) => <img src={obj.image} className={classes.img} />)}
          </Box>
        )}
        {!isMobile && (
          <Box display="flex" flexDirection="column-reverse">
            {images &&
              images
                .slice(6, 7)
                .map((obj) => <img src={obj.image} className={classes.img} />)}
          </Box>
        )}
        {!isMobile && (
          <Box display="flex" flexDirection="column-reverse">
            {images &&
              images
                .slice(7, 9)
                .map((obj) => <img src={obj.image} className={classes.img} />)}
          </Box>
        )}
        <Box display="flex" flexDirection="column-reverse">
          {images &&
            images
              .slice(9, 12)
              .map((obj) => <img src={obj.image} className={classes.img} />)}
        </Box>
        <Box display="flex" flexDirection="column-reverse">
          {images &&
            images
              .slice(12, 17)
              .map((obj) => <img src={obj.image} className={classes.img} />)}
        </Box>
      </Grid>
      {/* // </BRContainer> */}
    </>
  );
};
