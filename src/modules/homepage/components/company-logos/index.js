import React from "react";
import {
  Button,
  Grid,
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

import { CMS_PAGE_NAMES, useCMS } from "src/cms";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#F6F6F6",
    width: "auto",
    paddingBottom: "3.75vw",
    paddingTop: "2.7vw",
    minHeight: "22vw",
    [theme.breakpoints.down("sm")]: {
      padding: "22.22vw 5.55vw",
    },
    [theme.breakpoints.up("xl")]: {
      paddingTop: "2.03vw",
      paddingBottom: "3.33vw",
    },
  },
  parentGrid: {
    margin: "1.1vw 13.54vw 2.2vw",
    backgroundColor: "#F6F6F6",
    [theme.breakpoints.down("sm")]: {
      margin: 0,
      marginBottom: "5vw",
    },
  },
  logo: {
    textAlign: "center",
  },
  logoImage: {
    width: "100%",
  },

  center: {
    margin: "auto",
  },
  end: {
    textAlign: "end",
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
  },
  button: {
    width: 310,
    height: 48,
    borderRadius: 6,
    [theme.breakpoints.up("xl")]: {
      width: 288,
      height: 56,
    },
  },
}));

export const CompanyLogos = (props) => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles();

  const logos = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["company_logos"]);
  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      className={classes.root}
    >
      <Grid item xs="auto">
        <Typography variant="h5" color="primary" gutterBottom={isMobile}>
          Trusted by industry leaders
        </Typography>
      </Grid>
      <Grid
        container
        item
        direction="row"
        justify="space-around"
        alignItems="center"
        spacing={10}
        className={classes.parentGrid}
      >
        {logos &&
          logos.map(({ logo }, idx) => {
            return (
              <Grid item key={idx} className={classes.logo} xs={6} md={2}>
                <img src={logo} className={classes.logoImage} />
              </Grid>
            );
          })}
      </Grid>
      <Grid item xs="auto">
        <Button
          variant="contained"
          color="primary"
          onClick={props.onClick}
          className={classes.button}
        >
          Book A Demo
        </Button>
      </Grid>
    </Grid>
  );
};
