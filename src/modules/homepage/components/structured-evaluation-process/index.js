import {
  Box,
  Grid,
  List,
  ListItem,
  makeStyles,
  Typography,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import React from "react";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      width: "auto",
      display: "flex",
      justifyContent: "space-around",
      margin: "48px 68px 0 87px",
      "& li": {
        padding: 0,
      },
      [theme.breakpoints.up("xl")]: {
        margin: "48px 340px 0 341px",
      },
      [theme.breakpoints.down("sm")]: {
        margin: 0,
        padding: "0 22px",
      },
    },
    img: {
      [theme.breakpoints.up("sm")]: {
        width: 544,
        float: "left",
      },
    },
    rightSide: {
      margin: "auto",
      color: theme.palette.grey[900],
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        margin: "auto",
        paddingTop: 83,
      },
    },
    checkBox: {
      marginTop: "1.25vh",
      marginRight: "1.25vh",
    },
  };
});

export const StructuredEvaluationProcess = () => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isLarge = useMediaQuery(theme.breakpoints.up("xl"));
  const classes = useStyles();

  const texts =
    getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["streval_points"]) || [];
  return (
    <Grid
      container
      className={classes.root}
      justify="flex-start"
      align={isMobile ? "start" : "center"}
    >
      {!isMobile && (
        <Grid item sm={6}>
          <img
            src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["evaluation"])}
            className={classes.img}
          />
        </Grid>
      )}

      <Grid
        item
        xs={12}
        sm={6}
        justify="flex-start"
        wrap
        className={classes.rightSide}
      >
        <Typography
          variant={isMobile ? "h4" : "h3"}
          gutterBottom
          color="inherit"
          align="left"
        >
          {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
            "structured_evaluation_header",
          ])}
        </Typography>

        <List disablePadding>
          {texts.map(({ text }) => (
            <ListItem disableGutters disablePadding>
              <Grid
                container
                justify={isMobile ? "space-around" : "flex-start"}
              >
                <Grid item className={classes.checkBox}>
                  <img
                    src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                      "check_icon",
                    ])}
                  />
                </Grid>
                <Grid item xs={10}>
                  <Box color={theme.palette.grey[500]}>
                    <Typography variant="body1" color="inherit" gutterBottom>
                      {text}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </ListItem>
          ))}
        </List>
        {/* <Box textAlign="center">
            <Button variant="outlined" color="primary" size="large">
              Learn More
            </Button>
          </Box> */}
      </Grid>
    </Grid>
  );
};
