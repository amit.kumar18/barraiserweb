import {
  Avatar,
  Card,
  makeStyles,
  Typography,
  Container,
  Box,
  Grid,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { useCMS, CMS_PAGE_NAMES } from "src/cms";
import React from "react";

const useStyles = (image) =>
  makeStyles((theme) => ({
    root: {
      backgroundImage: `url(${image})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      padding: "104px 18.75vw 100px 19.9vw",
      backgroundColor: theme.palette.primary.main,
      backgroundPositionY: 30,
      [theme.breakpoints.up("xl")]: {
        padding: "104px 405px 80px",
        backgroundPositionY: -120,
      },
      [theme.breakpoints.down("lg")]: {
        paddingLeft: "15vw",
        paddingRight: "10vw",
        "& .parentGrid": {
          maxWidth: 1200,
          margin: "auto",
        },
      },
      [theme.breakpoints.down("sm")]: {
        padding: "90px 22px 79px 23px",
        backgroundImage: "none",
      },
    },
    cardContainer: {
      marginTop: "-2vw",
    },
    card: {
      height: 220,
      maxWidth: 540,
      [theme.breakpoints.down("sm")]: {
        margin: "auto",
        minHeight: 304,
        height: "auto",
        width: "auto",
        maxWidth: 315,
      },
    },
    arrowDown: {
      width: "0",
      height: "0",
      borderLeft: "18px solid transparent",
      borderRight: "18px solid transparent",
      borderTop: "18px solid #fff",
      position: "relative",
      left: "45%",
      marginTop: "-0.5%",
      marginBottom: 27,
      [theme.breakpoints.down("sm")]: {},
    },
    quoteMark: {
      float: "right",
      "& img": {
        filter: "brightness(100)",
        transform: "rotate(-180deg)",
        height: 149,
      },
    },
  }));

export const Candidatesays = () => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isRegular = useMediaQuery(theme.breakpoints.down("xl"));
  const classes = useStyles(
    getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["candidate_says_background"])
  )();
  return (
    <Container maxWidth={false} className={classes.root}>
      <Box
        align="center"
        color="white"
        width={isMobile ? "100%" : 643}
        pb={isMobile ? 8 : "auto"}
        m="auto"
        maxWidth={643}
      >
        <Typography variant="h2" color="inherit" gutterBottom>
          {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
            "candidate_says_header",
          ])}
        </Typography>
      </Box>

      {isMobile ? null : (
        <Box
          position="relative"
          className={classes.quoteMark}
          mt={"-6vw"}
          mb={"-2vw"}
        >
          <img src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, ["quote"])} />
        </Box>
      )}

      <Grid
        container
        className={classes.cardContainer}
        spacing={4}
        justify="center"
      >
        <Grid item sm={6}>
          <Card variant="outlined" className={classes.card}>
            <Typography variant="h5" align="center">
              <Box pt={isMobile ? 8 : 6} pb={4} color={theme.palette.grey[900]}>
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_says_card_1_title",
                ])}
              </Box>
            </Typography>
            <Typography variant="body2" align="center" color="textPrimary">
              <Box pl={6} pr={6} pb={5}>
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_says_card_1_text",
                ])}
              </Box>
            </Typography>
          </Card>
          <div className={`${classes.arrowDown} ${classes.leftSide}`}></div>

          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            pb={isMobile ? 17.5 : "auto"}
          >
            <Avatar
              src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                "candidate_says_avatar_1",
              ])}
            />
            <Box marginLeft="1rem">
              <Typography variant="subtitle1">
                <Box color="white">
                  {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                    "candidate_name_1",
                  ])}
                </Box>
              </Typography>
              <Typography variant="body2">
                <Box color="white" opacity={0.15}>
                  {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                    "candidate_subtitle_1",
                  ])}
                </Box>
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Card variant="outlined" className={classes.card}>
            <Typography variant="h5" align="center">
              <Box pt={isMobile ? 8 : 6} pb={4} color={theme.palette.grey[900]}>
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_says_card_2_title",
                ])}
              </Box>
            </Typography>
            <Typography variant="body2" align="center" color="textPrimary">
              <Box pl={6} pr={6} pb={5}>
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_says_card_2_text",
                ])}
              </Box>
            </Typography>
          </Card>
          <div className={`${classes.arrowDown} ${classes.rightSide}`}></div>
          <Box display="flex" justifyContent="center" alignItems="center">
            <Avatar
              src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                "candidate_says_avatar_2",
              ])}
            />
            <Box marginLeft="1rem" color="white">
              <Typography variant="subtitle1" color="inherit">
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_name_2",
                ])}
              </Typography>
              <Typography variant="body2" color="inherit">
                {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                  "candidate_subtitle_2",
                ])}
              </Typography>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};
