import React from "react";
import {
  Avatar,
  Button,
  Card,
  Icon,
  makeStyles,
  Typography,
  Box,
  Grid,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import { BRCarousel } from "src/modules/components";

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: "2.5vh",
    [theme.breakpoints.up("xl")]: {
      marginBottom: "1.67vw",
    },
  },
  leftSection: {
    width: "100%",
    marginBottom: 24,
    maxWidth: 506,
    [theme.breakpoints.down("sm")]: {
      margin: "auto",
    },
  },
  rightSection: { width: "80%" },
  [theme.breakpoints.down("sm")]: {
    rightSection: "100%",
  },
  paragraph: {
    textAlign: "left",
  },
  card: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    background: "#FFFFFF",
    height: 250,
    borderRadius: 0,
  },
  expertName: {
    color: theme.palette.primary.main,
    margin: "8px 0 2px",
  },
  expertPosition: {
    color: theme.palette.grey[900],
    margin: "2px 0 0",
  },
  expertExperience: {
    margin: "4px 0 0",
    color: theme.palette.grey[500],
  },
  avatar: {
    width: 80,
    height: 80,
  },
}));
export const IndustryExpertSays = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isLarge = useMediaQuery(theme.breakpoints.up("xl"));
  const classes = useStyles();
  const { getContentFromCMS } = useCMS();

  const experts = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["experts"]);

  return (
    <Box
      p={
        isMobile
          ? "40px 24px"
          : isLarge
            ? "5.62vw 8.75vw 5.62vw 10vw"
            : "5.62vw 8.75vw 5.62vw 10vw"
      }
      mb={isMobile ? 20 : 0}
    >
      <Grid
        container
        justify={isMobile ? "center" : "space-around"}
        spacing={4}
      >
        <Grid
          item
          xs={12}
          sm={6}
          className={classes.leftSection}
          justify="center"
        >
          <Typography
            variant={isMobile ? "h4" : "h3"}
            className={classes.title}
          >
            <Box
              pt={10}
              color={
                isMobile ? theme.palette.grey[900] : theme.palette.grey[700]
              }
            >
              {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
                "expert_section_title",
              ])}
            </Box>
          </Typography>
          <Typography variant="body2">
            <Box color={theme.palette.grey[500]} mb={isLarge ? 10 : 6}>
              {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["expert_text_1"])}
              <br />
              <br />
              {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["expert_text_2"])}
            </Box>
          </Typography>

          {!isMobile && (
            <Box
              textAlign={isMobile ? "center" : "start"}
              ml={isMobile ? -4 : 0}
            >
              <Button
                variant="outlined"
                color="primary"
                size="large"
                onClick={() => {
                  window.location.href = "/expert-partner";
                }}
              >
                Learn More
              </Button>
            </Box>
          )}
        </Grid>
        {isMobile ? (
          <BRCarousel
            itemsPerStep={1}
            items={experts || [{}]}
            itemRenderer={({ name, position, experience, avatar }) => (
              <Card elevation={8} className={classes.card}>
                <Avatar src={avatar} sizes="128px" className={classes.avatar} />
                <Typography variant="subtitle1" className={classes.expertName}>
                  {name}
                </Typography>
                <Typography variant="body2" className={classes.expertPosition}>
                  {position}
                </Typography>
                <Typography
                  variant="caption"
                  className={classes.expertExperience}
                >
                  {experience}
                </Typography>
              </Card>
            )}
          />
        ) : (
          <Grid item sm={5} className={classes.rightSection}>
            <Grid container spacing={6} justify="center">
              {experts &&
                experts.map(({ name, position, experience, avatar }) => {
                  return (
                    <Grid item xs={6}>
                      <Card elevation={8} className={classes.card}>
                        <Avatar
                          src={avatar}
                          sizes="128px"
                          className={classes.avatar}
                        />
                        <Typography
                          variant="subtitle1"
                          className={classes.expertName}
                        >
                          {name}
                        </Typography>
                        <Typography
                          variant="body2"
                          className={classes.expertPosition}
                        >
                          {position}
                        </Typography>
                        <Typography
                          variant="caption"
                          className={classes.expertExperience}
                        >
                          {experience}
                        </Typography>
                      </Card>
                    </Grid>
                  );
                })}
            </Grid>
          </Grid>
        )}
      </Grid>
      {isMobile && (
        <Box textAlign={isMobile ? "center" : "start"} mt={isMobile ? 10 : 0}>
          <Button
            variant="outlined"
            color="primary"
            size="large"
            onClick={() => {
              window.location.href = "/expert-partner";
            }}
          >
            Learn More
          </Button>
        </Box>
      )}
    </Box>
  );
};
