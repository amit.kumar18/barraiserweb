import {
  Grid,
  Typography,
  Button,
  useMediaQuery,
  useTheme,
  makeStyles,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import { BRContainer } from "modules/components";
import React from "react";

const useStyles = makeStyles((theme) => ({
  img1: {
    height: 250,
    marginLeft: "auto",
    display: "block",
    [theme.breakpoints.down("sm")]: {
      height: 350,
    },
  },
  img2: {
    height: 230,
    marginTop: -72,
    position: "relative",
    display: "block",
    marginLeft: "auto",
    marginRight: -75,
    [theme.breakpoints.down("sm")]: {
      height: 183,
      marginRight: -111,
      marginTop: -125,
    },
  },
  imgParent: {
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "11.38vw",
    },
  },
  root: {
    padding: "5.2vw 0 4.16vw 14.09vw",
    [theme.breakpoints.down("sm")]: {
      padding: "22.22vw 0 12.5vw",
    },
    [theme.breakpoints.up("xl")]: {
      padding: "3.81vw 18.12vw 5.83vw 17.7vw",
    },
  },
  header: {
    color: theme.palette.grey[900],
    maxWidth: 682,
    paddingBottom: 24,
    [theme.breakpoints.up("xl")]: {
      padding: "0.5vw 3vw 1.67vw 0",
      maxWidth: 556,
    },
    [theme.breakpoints.down("sm")]: {
      color: theme.palette.grey[900],
      paddingBottom: 0,
    },
  },
  left: {
    [theme.breakpoints.down("sm")]: {
      padding: "0 6.67vw",
    },
  },
  button: {
    [theme.breakpoints.down("sm")]: {
      marginBottom: 60,
    },
  },
}));

export const CaseStudy = () => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles();

  return (
    <BRContainer
      maxWidth={false}
      backgroundImage={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
        "case_studies",
      ])}
      backgroundPosition="inherit"
      className={classes.root}
    >
      <Grid container justify={isMobile ? "center" : "space-between"}>
        <Grid item xs={12} sm={7} className={classes.left}>
          <Typography
            variant={isMobile ? "h4" : "h3"}
            className={classes.header}
            gutterBottom
          >
            {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
              "case_studies_header",
            ])}
          </Typography>

          <Button
            variant="outlined"
            color="primary"
            size="large"
            onClick={() => {
              window.location.href = "/about-us";
            }}
            className={classes.button}
            fullWidth={isMobile}
          >
            Learn More
          </Button>
        </Grid>
        <Grid item xs={12} sm={5} className={classes.imgParent}>
          <img
            src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
              "featured_image_1",
            ])}
            className={classes.img1}
          />
          <img
            src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
              "featured_image_2",
            ])}
            className={classes.img2}
          />
        </Grid>
      </Grid>
    </BRContainer>
  );
};
