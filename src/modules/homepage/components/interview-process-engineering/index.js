import {
  Box,
  Button,
  Card,
  Container,
  Grid,
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#F9F9F9",
    [theme.breakpoints.down("sm")]: {
      paddingBottom: 80,
    },
  },
  buttonContainer: {
    textAlign: "center",
  },
  header: {
    paddingTop: 64,
    paddingBottom: 79,
    textAlign: "center",
    color: theme.palette.grey[900],
    maxWidth: 684,
    margin: "auto",
    [theme.breakpoints.down("sm")]: {
      paddingTop: 64,
      paddingBottom: 24,
      textAlign: "start",
      color: theme.palette.grey[900],
      width: 284,
    },
    [theme.breakpoints.up("xl")]: {
      maxWidth: 918,
      paddingTop: 80,
      paddingBottom: 44,
    },
  },
  title: {
    textAlign: "start",
    color: "#000",
    "& span": {
      color: theme.palette.primary.main,
    },
  },
  processImage: {
    display: "block",
    margin: "auto",
    position: "relative",
    top: 70,
    right: 20,
  },
  parentGrid: {
    width: 1240,
    position: "relative",
    margin: "auto",
    marginBottom: "-13vw",
    [theme.breakpoints.down("sm")]: {
      width: "auto",
      marginBottom: 0,
      padding: "0 24px",
    },
    zIndex: 5,
  },
  card: {
    padding: "24px 26px 28px 28px",
    height: 118,
    maxWidth: 447,
    borderRadius: 8,
    [theme.breakpoints.down("sm")]: {
      height: "auto",
      width: "auto",
      margin: "auto",
      marginBottom: 22,
    },
  },
  lastCard: {
    marginTop: 35,
    margin: "auto",
  },
}));

export const InterviewProcessEngineering = ({ onClick }) => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isLarge = useMediaQuery(theme.breakpoints.up("xl"));
  const classes = useStyles();

  const processes = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "process_cards",
  ]);

  return (
    <Container maxWidth={false} className={classes.root}>
      <Typography variant={isMobile ? "h4" : "h3"} className={classes.header}>
        {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["process_header"])}
      </Typography>
      <Grid
        container
        justify={isMobile ? "center" : "space-between"}
        className={classes.parentGrid}
      >
        {processes &&
          processes.map(({ title, para }, idx) => (
            <Grid item xs={12} sm={idx === 2 ? 12 : "auto"}>
              <Card
                elevation={8}
                className={
                  idx === 2 && !isMobile
                    ? `${classes.lastCard} ${classes.card}`
                    : classes.card
                }
              >
                <Typography
                  variant="h5"
                  className={classes.title}
                  dangerouslySetInnerHTML={{ __html: title }}
                ></Typography>
                <Typography variant="body2">
                  <Box pt={4}>{para}</Box>
                </Typography>
              </Card>
            </Grid>
          ))}
      </Grid>

      {isMobile ? null : (
        <img
          src={getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
            "process_engineering",
          ])}
          className={classes.processImage}
        />
      )}

      <Box
        className={classes.buttonContainer}
        pt={isMobile ? 4 : 25}
        pb={isLarge ? 20 : isMobile ? 0 : 16}
      >
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={onClick}
        >
          Book a demo
        </Button>
      </Box>
    </Container>
  );
};
