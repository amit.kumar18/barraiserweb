import {
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
  Box,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#F9F9F9",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
  },
  paragraph: {
    marginTop: 24,
    width: "42.5vw",
    [theme.breakpoints.down("sm")]: {
      width: "80vw",
    },
  },
  button: {
    padding: "0.5rem 1.25rem",
    width: "21.8rem",
    height: "4rem",
    background: "#0898A9",
    borderRadius: "0.4rem",
    marginTop: "2.5vh",
  },
}));

export function CheckoutSection(props) {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isLarge = useMediaQuery(theme.breakpoints.up("xl"));
  const classes = useStyles();
  return (
    <Box className={classes.root} pt={isLarge ? 20 : 10} pb={isLarge ? 20 : 10}>
      <Typography variant="h4" color="primary" className={classes.header}>
        {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["checkout_title"])}
      </Typography>
      <Typography variant="body2" align="center" className={classes.paragraph}>
        {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, ["checkout_para"])}
      </Typography>
      {/* <Button variant="contained" color="primary" className={classes.button}>
        Learn More
      </Button> */}
    </Box>
  );
}
