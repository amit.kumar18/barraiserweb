import {
  Box,
  makeStyles,
  Typography,
  Grid,
  useTheme,
  useMediaQuery,
  Avatar,
  Container,
  Card,
} from "@material-ui/core";
import { CMS_PAGE_NAMES, useCMS } from "src/cms";
import { BRCarousel } from "src/modules/components";
import React from "react";

const useStyles = makeStyles((theme) => {
  console.log("theme.palette.primary.main: ", theme.palette.primary.main)
  return {
    root: {
      background: theme.palette.grey[50],
      padding: "6.11vw 13.54vw",
      [theme.breakpoints.down("sm")]: {
        padding: "18vw 5vw 0",
      },
      [theme.breakpoints.up("xl")]: {
        padding: "5.46vw 17.95vw",
      },
      position: "relative",
    },
    container: {
      [theme.breakpoints.down("sm")]: {
        height: "auto",
        paddingBottom: 56,
      },
    },
    title: {
      margin: "-64px 0vw 0vh 74px",
      width: 323,
      color: theme.palette.grey[700],
      [theme.breakpoints.down("sm")]: {
        margin: "-6vh 0vw 0vh 7vw",
        color: theme.palette.grey[900],
        width: "75.27vw",
      },
    },
    parentGrid: {
      paddingLeft: "25vw",
    },
    flexWrap: {
      display: "flex",
      flexWrap: "wrap",
    },
    paragraph: {
      marginTop: "24px",
      color: theme.palette.grey[500],
      [theme.breakpoints.down("sm")]: {
        marginBottom: 24,
        paddingLeft: 15,
      },
    },
    widthAuto: {
      width: "auto",
    },
    imgSize: {
      height: 40,
    },
    card: {
      width: 330,
      float: "right",
      padding: 32,
      paddingRight: 10,
      marginBottom: 25,
      [theme.breakpoints.down("sm")]: {
        width: 275,
        margin: "auto",
        float: "none",
      },
      [theme.breakpoints.up("xl")]: {
        width: 370,
      },
    },
    quoteBox: {
      position: "absolute",
      "& img": {
        opacity: 0.2,
        height: 120,
      },
      [theme.breakpoints.down("sm")]: {
        marginLeft: "-5vw",
        marginBottom: 37,
        position: "relative",
      },
    },
    topSpace: {
      position: "relative",
      top: 252,
    },
    rightCards: {
      paddingLeft: 24,
      "&>div": {
        float: "left",
      },
    },
    smallQuote: {
      height: 14,
      marginTop: 20,
      color: "#2EC5CE",
      [theme.breakpoints.down("sm")]: {
        marginTop: 0,
      },
    },
  };
});
export const ClientTestimonials = () => {
  const { getContentFromCMS } = useCMS();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  const classes = useStyles();

  const testimonials = getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
    "client_testimonial",
  ]);

  return (
    <Box className={classes.root}>
      <Container maxWidth={false} className={classes.container}>
        <Box className={classes.quoteBox}>
          <img src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, ["quote"])} />
          <Typography
            variant={isMobile ? "h4" : "h3"}
            className={classes.title}
          >
            {getContentFromCMS(CMS_PAGE_NAMES.HOME_PAGE, [
              "client_testimonial_header",
            ])}
          </Typography>
        </Box>
        {isMobile && testimonials ? (
          <BRCarousel
            itemsPerStep={1}
            items={testimonials}
            itemRenderer={({ logo, text, photo, name, position }, idx) => {
              return (
                <Card elevation={8} className={classes.card}>
                  <img src={logo} className={classes.imgSize} />

                  <Grid container spacing={1}>
                    <Grid item xs={1}>
                      <Box mt={5}>
                        <img
                          src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, [
                            "quote",
                          ])}
                          className={classes.smallQuote}
                        />
                      </Box>
                    </Grid>
                    <Grid item xs={10}>
                      <Typography variant="body2" className={classes.paragraph}>
                        {text}
                      </Typography>
                      <Box
                        display="flex"
                        justifyContent="start"
                        height="10vh"
                        alignItems="center"
                      >
                        <Avatar src={photo} />
                        <Box marginLeft="1rem" color={theme.palette.grey[900]}>
                          <Typography variant="subtitle1" color="inherit">
                            {name}
                          </Typography>
                          <Typography variant="body2" color="textPrimary">
                            {position}
                          </Typography>
                        </Box>
                      </Box>
                    </Grid>
                  </Grid>
                </Card>
              );
            }}
          />
        ) : (
          <Grid container justify="flex-end" className={classes.parentGrid}>
            {testimonials &&
              testimonials.map(({ logo, text, photo, name, position }, idx) => {
                return (
                  <Grid
                    item
                    xs={6}
                    className={!idx ? classes.topSpace : classes.rightCards}
                  >
                    <Card elevation={8} className={classes.card}>
                      <img src={logo} className={classes.imgSize} />

                      <Grid container spacing={1}>
                        <Grid item xs={1}>
                          <img
                            src={getContentFromCMS(CMS_PAGE_NAMES.COMMON, [
                              "quote",
                            ])}
                            className={classes.smallQuote}
                          />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography
                            variant="body2"
                            className={classes.paragraph}
                          >
                            {text}
                          </Typography>
                          <Box
                            display="flex"
                            justifyContent="start"
                            height="10vh"
                            alignItems="center"
                          >
                            <Avatar src={photo} />
                            <Box
                              marginLeft="1rem"
                              color={theme.palette.grey[900]}
                            >
                              <Typography variant="subtitle1" color="inherit">
                                {name}
                              </Typography>
                              <Typography variant="body2" color="textPrimary">
                                {position}
                              </Typography>
                            </Box>
                          </Box>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                );
              })}
          </Grid>
        )}
      </Container>
    </Box>
  );
};
