export const WorkExperienceOptions = [
  {
    value: "ONE_TWO_YEARS",
    label: "1 - 2 years",
  },
  {
    value: "TWO_THREE_YEARS",
    label: "2 - 3 years",
  },
  {
    value: "THREE_FIVE_YEARS",
    label: "3 - 5 years",
  },
  {
    value: "FIVE_NINE_YEARS",
    label: "5 - 9 years",
  },
  {
    value: "NINE_TWELVE_YEARS",
    label: "9 - 12 years",
  },
  {
    value: "MORE_THAN_TWELEVE_YEARS",
    label: "> 12 years",
  },
];

export const CTCOptions = [
  {
    value: "ZERO_THREE_LACS",
    label: "0 - 3 lacs",
  },
  {
    value: "THREE_FIVE_LACS",
    label: "3 - 5 lacs",
  },
  {
    value: "FIVE_EIGHT_LACS",
    label: "5 - 8 lacs",
  },
  {
    value: "EIGHT_TWELVE_LACS",
    label: "8 - 12 lacs",
  },
  {
    value: "TWELEVE_TWENTY_LACS",
    label: "12 - 20 lacs",
  },
  {
    value: "TWENTY_THIRTY_LACS",
    label: "20 - 30 lacs",
  },
  {
    value: "THIRTY_FOURTY_LACS",
    label: "30 - 40 lacs",
  },
  {
    value: "MORE_THAN_FOURTY_LACS",
    label: "> 40 lacs",
  },
];

export const START_APPLYING_PERIODS = [
  {
    value: "ONE_TWO_WEEKS",
    label: "1 - 2 weeks",
  },
  {
    value: "TWO_THREE_WEEKS",
    label: "2 - 3 weeks",
  },
  {
    value: "THREE_FIVE_WEEKS",
    label: "3 - 5 weeks",
  },
  {
    value: "FIVE_NINE_WEEKS",
    label: "5 - 9 weeks",
  },
  {
    value: "NINE_TWELVE_WEEKS",
    label: "9 - 12 weeks",
  },
  {
    value: "MORE_THAN_TWELEVE_WEEKS",
    label: "> 12 weeks",
  },
];

export const DIFFICULTY_COLOR = {
  VERY_EASY: "#475ae5",
  EASY: "#6c7bea",
  MODERATE: "#9aa6ff",
  HARD: "#0b008c",
  VERY_HARD: "#0b008c",
  "N.A": "#979797",
};

export const RATING_COLOR = [
  // 0 (rating does not exist)
  null,
  // 1, 2
  "#D50000",
  "#D50000",
  // 3, 4
  "#D32F2F",
  "#D32F2F",
  // 5, 6
  "#E54304",
  "#E54304",
  // 7, 8
  "#75E900",
  "#75E900",
  // 9, 10
  "#008B00",
  "#008B00",
];

export const RATING_STANDARDS_CONFIGS = [
  {
    displayText: "Did Not Answer",
    ratingRange: [1, 2],
    description: ["No answer", "No attempt"],
    baseColor: "#D50000",
  },
  {
    displayText: "Doesn't Meet Expectations",
    ratingRange: [3, 4],
    description: [
      "Incorrect answer",
      "Hints not picked",
      "Below average response time",
    ],
    baseColor: "#D32F2F",
  },
  {
    displayText: "Somewhat Meets Expectations",
    ratingRange: [5, 6],
    description: [
      "Incomplete answer",
      "Bad at picking hints",
      "Below average response time",
    ],
    baseColor: "#E54304",
  },
  {
    displayText: "Meets Expectations",
    ratingRange: [7, 8],
    description: [
      "Correct answer",
      "Good at picking hints",
      "Average response time",
    ],
    baseColor: "#75E900",
  },
  {
    displayText: "Exceeds Expectations !",
    ratingRange: [9, 10],
    description: [
      "Perfect answer",
      "No hints required",
      "Above average response time",
    ],
    baseColor: "#008B00",
  },
];

export const mapInterviewStatus = (status) => {
  const mapping = {
    pending_scheduling: "Pending Scheduling",
    pending_ta_assignment: "Scheduled",
    pending_interviewing: "Scheduled",
    pending_tagging: "Interviewed",
    pending_decision: "Incomplete Interview",
    pending_feedback_submission: "Interviewed",
    pending_qc: "Interviewed",
    pending_correction: "Interviewed",
    cancellation_done: "Cancelled",
    Done: "Done",
    PENDING_DATA_ENTRY: "Pending Scheduling",
    PENDING_DATA_ENTRY_2: "Pending Scheduling",
  };

  return mapping[status];
};
