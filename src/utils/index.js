export const isDevBuild = () => {
  return process.env.NODE_ENV !== "production";
};

export function safeGet(obj = {}, path) {
  return Array.prototype.reduce.call(
    path,
    (object, el) => {
      if (object !== null && object !== undefined) {
        if (typeof el === "string" && el.trim() === "") {
          return object;
        }
        return object[el];
      }
    },
    obj
  );
}

export const safeGetViaSchema = (obj, getSchema = {}) => {
  if (Object.prototype.toString.call(obj) !== "[object Object]") {
    return JSON.parse(JSON.stringify(getSchema));
  }

  const objCopy = JSON.parse(JSON.stringify(obj));
  const returnPart = Object.keys(getSchema).reduce((agg, key) => {
    let pickObj = getSchema;
    if (
      Object.prototype.toString.call(obj[key]) ===
      Object.prototype.toString.call(getSchema[key])
    ) {
      pickObj = obj;
    }
    agg[key] = JSON.parse(
      JSON.stringify({
        [key]: pickObj[key],
      })
    )[key];

    delete objCopy[key];
    return agg;
  }, {});

  return {
    ...obj,
    ...returnPart,
  };
};

export const omitKeys = (obj, keysArr) => {
  if (!Array.isArray(keysArr)) {
    return obj;
  }
  return keysArr.reduce(
    (agg, currentVal) => {
      delete agg[currentVal];
      return agg;
    },
    { ...obj }
  );
};

export const stripTypeNameKey = (obj) => {
  if (Object.prototype.toString.call(obj) !== "[object Object]") {
    return {};
  }

  return omitKeys(obj, ["__typename"]);
};

export const stripTypeNameKeyDeep = (obj) => {
  if (Object.prototype.toString.call(obj) !== "[object Object]") {
    return {};
  }

  let returnObj = stripTypeNameKey(obj);
  return Object.keys(returnObj).reduce(
    (agg, currentKey) => {
      const typeDetectStr = Object.prototype.toString.call(agg[currentKey]);
      if (typeDetectStr === "[object Object]") {
        agg[currentKey] = stripTypeNameKeyDeep(agg[currentKey]);
      } else if (typeDetectStr === "[object Array]") {
        agg[currentKey] = agg[currentKey].map(stripTypeNameKeyDeep);
      }
      return agg;
    },
    { ...returnObj }
  );
};

export function isValidPath(obj, path) {
  if (!(path instanceof Array) || path.length === 0) {
    return false;
  }
  const deepObj = safeGet(obj, path.slice(0, path.length - 1));
  if (deepObj) {
    return Object.hasOwnProperty.call(deepObj, path[path.length - 1]);
  }
  return false;
}

export function isMobileScreenSize() {
  return document.body.clientWidth <= 768;
}

export function momentToSeconds(obj) {
  const hoursInSeconds = obj.hours() * 3600;
  const minutesInSeconds = obj.minutes() * 60;
  const seconds = obj.seconds();

  return hoursInSeconds + minutesInSeconds + seconds;
}

export function convertMonthsToYears(monthsInit, formatter) {
  const defaultFormatter = (years = 0, months = 0) => {
    const yearsString = years ? `${years} yr${years > 1 ? "s" : ""}` : "";
    const monthsString = months ? `${months} mo${years > 1 ? "s" : ""}` : "";
    return [yearsString, monthsString].filter((a) => a).join(" ");
  };

  const currentFormatter = formatter || defaultFormatter;
  const yrs = Math.floor(monthsInit / 12);
  const mnths = monthsInit % 12;

  return currentFormatter(yrs, mnths);
}

export const prefixObjectValues = (prefix = "", obj) => {
  var retObj = {};
  if (typeof obj !== "object") {
    return retObj;
  }

  Object.keys(obj).map((key) => {
    retObj[key] = `${prefix}${obj[key]}`;
  });

  return retObj;
};

export const isValidEmail = (email) => {
  return email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
};

export const isValidPhone = (phone) => {
  return phone.match(
    /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
  );
};

export const isStringEmpty = (string) => string === null || string === "";

export * from "./debounce";
export * from "./routeUtils";
