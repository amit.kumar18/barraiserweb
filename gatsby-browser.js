import React from "react";
import RootLayout from "./src/RootLayout";

import "@fontsource/open-sans";
import "@fontsource/roboto";
import "@fontsource/montserrat";


export const wrapRootElement = ({ element }) => <RootLayout>{element}</RootLayout>;
